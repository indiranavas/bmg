package com.allcom.bmgestweb.domain;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "store")
public class Local {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "description")
    private String description;
 
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "store")
    private List<Scale> listScale;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Scale> getListScale() {
        return listScale;
    }

    public void setListScale(List<Scale> listScale) {
        this.listScale = listScale;
    }
}
