
package com.allcom.bmgestweb.domain;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "activity")
public class Activity {
    
    @Id
    @Column(name = "id")
    private int id;

    @Column(name = "component")
    private String component;

    @Column(name = "date")
    private Date date;
    
     @Column(name = "nameUser")
    private String nameUser;
    
//     @JoinColumn(name = "idUser")
//    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    private User user;
}
