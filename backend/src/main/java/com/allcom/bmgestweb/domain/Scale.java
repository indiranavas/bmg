package com.allcom.bmgestweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "scale")
public class Scale {
    
 @EmbeddedId
 private ScaleId scaleId;

    public ScaleId getScaleId() {
        return scaleId;
    }

    public void setScaleId(ScaleId scaleId) {
        this.scaleId = scaleId;
    }

    @Column(name = "description")
    private String description;

    @Column(name = "ip")
    private String ip;
    
    @Column(name = "status")
    private boolean status;

    @JsonIgnore 
    @JoinColumn(name = "idstore",nullable = false, updatable = false, insertable = false)
    @ManyToOne(optional = false)
    private Local store;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Local getStore() {
        return store;
    }

    public void setStore(Local store) {
        this.store = store;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }    
   

}
