package com.allcom.bmgestweb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "accessstatistics")
    private int accessStatistics;

    @Column(name = "accessstore")
    private int accessStore;

    @Column(name = "accesslayout")
    private int accessLayout;

    @Column(name = "accessupdatescale")
    private int accessUpdateScale;

    @Column(name = "accessproduct")
    private int accessProduct;

    @Column(name = "accessfamily")
    private int accessFamily;

    @Column(name = "accessadministration")
    private int accessAdministration;

    @Column(name = "accessactivity")
    private int accessActivity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAccessStatistics() {
        return accessStatistics;
    }

    public void setAccessStatistics(int accessStatistics) {
        this.accessStatistics = accessStatistics;
    }

    public int getAccessStore() {
        return accessStore;
    }

    public void setAccessStore(int accessStore) {
        this.accessStore = accessStore;
    }

    public int getAccessLayout() {
        return accessLayout;
    }

    public void setAccessLayout(int accessLayout) {
        this.accessLayout = accessLayout;
    }

    public int getAccessUpdateScale() {
        return accessUpdateScale;
    }

    public void setAccessUpdateScale(int accessUpdateScale) {
        this.accessUpdateScale = accessUpdateScale;
    }

    public int getAccessProduct() {
        return accessProduct;
    }

    public void setAccessProduct(int accessProduct) {
        this.accessProduct = accessProduct;
    }

    public int getAccessFamily() {
        return accessFamily;
    }

    public void setAccessFamily(int accessFamily) {
        this.accessFamily = accessFamily;
    }

    public int getAccessAdministration() {
        return accessAdministration;
    }

    public void setAccessAdministration(int accessAdministration) {
        this.accessAdministration = accessAdministration;
    }

    public int getAccessActivity() {
        return accessActivity;
    }

    public void setAccessActivity(int accessActivity) {
        this.accessActivity = accessActivity;
    }
    
}
