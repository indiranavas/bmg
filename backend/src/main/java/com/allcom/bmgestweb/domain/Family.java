package com.allcom.bmgestweb.domain;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "family")
public class Family {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @ManyToMany(mappedBy = "listFamily")
    private List<Layout> listLayout;

    @ManyToMany()
    @JoinTable(
            name = "productbyfamily",
            joinColumns = {@JoinColumn(name = "idfamily")},
            inverseJoinColumns = {@JoinColumn(name = "idproduct")}
    )
    private List<Product> listProduct;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<Layout> getListLayout() {
        return listLayout;
    }

    public void setListLayout(List<Layout> listLayout) {
        this.listLayout = listLayout;
    }

    public List<Product> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<Product> listProduct) {
        this.listProduct = listProduct;
    }

}
