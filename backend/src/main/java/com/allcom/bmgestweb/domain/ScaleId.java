
package com.allcom.bmgestweb.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ScaleId implements Serializable{
    
     @Column(name = "id")   
    private int id;
     
     @Column(name = "idstore")   
    private String idStore; 

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdStore() {
        return idStore;
    }

    public void setIdStore(String idStore) {
        this.idStore = idStore;
    }
    
     @Override
    public int hashCode() {
        return (int) (id + idStore.hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ScaleId)) {
            return false;
        }
        if (obj == null) {
            return false;
        }
        ScaleId pk = (ScaleId) obj;
        return pk.id==id && pk.idStore.equals(idStore);
    }
}
