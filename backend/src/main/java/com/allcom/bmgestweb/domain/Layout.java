package com.allcom.bmgestweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "layout")
public class Layout {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @ManyToMany()
    @JoinTable(
            name = "familybylayout",
            joinColumns = {
                @JoinColumn(name = "idlayout")},
            inverseJoinColumns = {
                @JoinColumn(name = "idfamily")}
    )
    private List<Family> listFamily;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public List<Family> getListFamily() {
        return listFamily;
    }

    public void setListFamily(List<Family> listFamily) {
        this.listFamily = listFamily;
    }

}
