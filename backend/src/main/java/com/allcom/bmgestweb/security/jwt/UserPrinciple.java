package com.allcom.bmgestweb.security.jwt;

import com.allcom.bmgestweb.domain.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserPrinciple implements UserDetails {

    private static final long serialVersionUID = 1L;

    private int id;

    private String name;

    private String lastName;

    private String username;

    private String email;

    @JsonIgnore
    private String password;
    
    private boolean status;
 

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrinciple(int id, String name, String lastName,
            String username, String password, boolean status,
            Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.status=status;
    }

    public static UserPrinciple build(User user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessActivity())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessFamily())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessLayout())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessProduct())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessStatistics())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessStore())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessUpdateScale())));
        authorities.add(new SimpleGrantedAuthority(String.valueOf(user.getRole().getAccessAdministration())));

        return new UserPrinciple(
                user.getId(),
                user.getName(),
                user.getLastName(),
                user.getUserLogin(),
                user.getPassword(),
                user.isStatus(),
                authorities
        );
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }
    
    public String getLastName() {
        return lastName;
    }

       public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserPrinciple user = (UserPrinciple) o;
        return Objects.equals(id, user.id);
    }
}
