package com.allcom.bmgestweb.rest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;

import com.sshtools.j2ssh.ScpClient;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.transport.IgnoreHostKeyVerification;

public class ScpConnect {

    private SshClient ssh;
    private PasswordAuthenticationClient pass;
    private String hostname;
    private String user;

    public ScpConnect(String hostname, String login, String password) {
        ssh = new SshClient();
        this.hostname = hostname;       
        pass = new PasswordAuthenticationClient();
        this.pass.setUsername(login);
        this.pass.setPassword(password);
    }

    public void sendFile(InputStream f, long size, String nombreLocal,
            String nombreRemoto) throws ConnectException {
        try {
            ssh.connect(hostname, new IgnoreHostKeyVerification());
            int ret = ssh.authenticate(pass);
            if (ret == 2) {
                 this.pass.setPassword("root");
                 ret=ssh.authenticate(pass);
            }
            if (ret == 4) {
                ScpClient scpClient = ssh.openScpClient();
                scpClient.put(f, size, nombreLocal, nombreRemoto);
                ssh.disconnect();
            } else {
                throw new ConnectException("Error en la autenticacion de usuario");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ConnectException("No se pudo conectar al Servidor = " + hostname);
        }
    }

    public boolean getFile(OutputStream destino, String ruta_origen)
            throws ConnectException {
        try {
            ssh.connect(hostname, new IgnoreHostKeyVerification());
            int ret = ssh.authenticate(pass);
            if (ret == 4) {
                ScpClient scpClient = ssh.openScpClient();
                InputStream stream = scpClient.get(ruta_origen);
                BufferedInputStream bis = new BufferedInputStream(stream);
                BufferedOutputStream bos = new BufferedOutputStream(destino);
                byte data[] = new byte[8024];
                int contador;
                while ((contador = bis.read(data, 0, 8024)) != -1) {
                    bos.write(data, 0, contador);
                }
                ssh.disconnect();
                return true;
            } else {
                throw new ConnectException("Error en la autenticacion de usuario");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable ex) {
            ex.printStackTrace();
        }
        throw new ConnectException("Error de Entrada/Salida");
    }
}
