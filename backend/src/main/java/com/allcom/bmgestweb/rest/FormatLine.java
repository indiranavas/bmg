package com.allcom.bmgestweb.rest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FormatLine {

    public List<String> format(List<String> data, String file, String method) {
        List<String> dataFormat = null;
        if (data != null) {
            dataFormat = new ArrayList<String>();
            Iterator<String> dataListIter = data.iterator();
            while (dataListIter.hasNext()) {
                String line = (String) dataListIter.next();
                String[] posData = line.split(",", -1);
                if (file.equals("Articulos") && (method.equals("Insertar") || method.equals("Actualizar"))) {
                    dataFormat.add(formatInsertArtigos(posData));
                } else if (file.equals("Familias") && (method.equals("Insertar"))) {
                    dataFormat.add(formatInsertFamilias(posData));
                }else if (file.equals("Familias")&& (method.equals("Actualizar"))){
                    dataFormat.add(formatUpdateFamilias(posData));
                } else if (file.equals("Acceso") && (method.equals("Insertar") || method.equals("Actualizar"))) {
                    dataFormat.add(formatInsertAcceso(posData));
                } else if (file.equals("Ingredientes") && (method.equals("Insertar") || method.equals("Actualizar"))) {
                    dataFormat.add(formatInsertIngredientes(posData));
                } else if (file.equals("Nutrientes") && (method.equals("Insertar") || method.equals("Actualizar"))) {
                    dataFormat.add(formatInsertNutrientes(posData));
                } else if (file.equals("ExtraArticulos") && (method.equals("Insertar") || method.equals("Actualizar"))) {
                    dataFormat.add(formatInsertExtraArticulos(posData));
                } else if (file.equals("Articulos") && method.equals("Eliminar")) {
                    dataFormat.add(formatDeleteArtigosandFamilias(posData));
                } else if (file.equals("Familias") && method.equals("Eliminar")) {
                    dataFormat.add(formatDeleteArtigosandFamilias(posData));
                } else if (file.equals("Acceso") && method.equals("Eliminar")) {
                    dataFormat.add(formatDeleteAcceso(posData));
                } else if (file.equals("Ingredientes") && method.equals("Eliminar")) {
                    dataFormat.add(formatDeleteIngredientesyNutrientes(posData));
                } else if (file.equals("Nutrientes") && method.equals("Eliminar")) {
                    dataFormat.add(formatDeleteIngredientesyNutrientes(posData));
                } else if (file.equals("Articulos") && method.equals("Mostrar")) {
                    dataFormat.add(formatGetArtigos(posData));
                }
            }
        }
        return dataFormat;
    }

    private String formatInsertArtigos(String[] posData) {
        int index = posData[1].indexOf("\"");
        if (index >= 0) {
            posData[1] = posData[1].replace("\"", "");
        }
        return "{\"codigo\":\"" + posData[0] + "\",\"designacao\":\"" + posData[1] + "\",\"img\":\"" + posData[2]
                + "\",\"desactivo\":\"" + posData[3] + "\",\"unidades\":\"" + posData[4] + "\",\"preco1\":\"" + posData[5]
                + "\",\"num_ing\":\"" + posData[0] + "\",\"num_etq\":\"" + posData[7] + "\",\"img_etq\":\"" + posData[6] + "\",\"num_nutr\":\"" + posData[0]
                + "\",\"preco4\":\"" + posData[8] + "\" }";

    }

    private String formatInsertFamilias(String[] posData) {
        return "{\"codigo\":\"" + posData[0] + "\",\"descricao\":\"" + posData[1] + "\",\"img\":\"" + posData[2] 
                + "\",\"dep\":\"" + posData[3] + "\",\"blacklist\":\"" + posData[4] + "\",\"tipo\":\"" + posData[5]
                + "\",\"categoria\":\"" + posData[6] + "\"}";
    }
    
        private String formatUpdateFamilias(String[] posData) {
        return "{\"codigo\":\"" + posData[0] + "\",\"blacklist\":\"" + posData[1] + "\"}";
    }

    private String formatInsertAcceso(String[] posData) {
        return "{\"cod_cab\":\"" + posData[1] + "\",\"linha_f\":\"" + posData[2] + "\",\"cod_art\":\"" + posData[0] + "\"}";
    }

    private String formatInsertIngredientes(String[] posData) {
         int index = posData[1].indexOf("\"");
        if (index >= 0) {
            posData[1] = posData[1].replace("\"", "");
        }
        StringBuilder line = new StringBuilder();
        line.append("{\"numero\":\"" + posData[0] + "\",\"designacao\":\"" + posData[1]);
        line.append("\",\"txt37" + "\":\"").append(posData[2].trim());
        line.append("\",\"txt38" + "\":\"").append(posData[3].trim());
        line.append("\",\"txt39" + "\":\"").append(posData[4].trim());
        line.append("\",\"txt40" + "\":\"").append(posData[5].trim());
        line.append("\",\"txt1" + "\":\"");
        for (int i = 6; i < posData.length - 1; i++) {
            line.append(posData[i].trim()).append(",");
        }
        line.append(posData[posData.length - 1].trim());
//        for (int i = 0; i < posData.length; i++) {
//            if ((i + 2) < posData.length) {
//                line.append("\",\"txt" + (i + 1) + "\":\"" + posData[i + 2]);
//            }
//        }
        line.append("\"}");
        return line.toString();
    }

    private String formatInsertNutrientes(String[] posData) {
        StringBuilder line = new StringBuilder();
        line.append("{\"numero\":\"" + posData[0] + "\",\"designacao\":\"" + posData[1]);
        line.append("\",\"designacao2\":\"" + posData[2]);
        for (int i = 0; i < posData.length; i++) {
            if (((3 * i) + 5) < posData.length) {
                line.append("\",\"txt" + (i) + "\":\"" + posData[(3 * i) + 3]);
                line.append("\",\"val_a" + (i) + "\":\"" + posData[(3 * i) + 4]);
                line.append("\",\"val_b" + (i) + "\":\"" + posData[(3 * i) + 5]);
            }
        }
        line.append("\"}");
        return line.toString();

    }

    private String formatInsertExtraArticulos(String[] posData) {
        return "{\"cab_lnh\":\"" + posData[0] + "\",\"num_ord\":\"" + posData[1] + "\",\"cab_txt\":\"" + posData[2] + "\"}";
    }

    private String formatDeleteArtigosandFamilias(String[] posData) {
        return "seek={\"codigo\":\"" + posData[0] + "\"}&limit=1";
    }

    private String formatDeleteAcceso(String[] posData) {
        return "seek={\"cod_cab\":\"" + posData[0] + "\",\"linha_f\":\"" + posData[1] + "\"}&limit=1";
    }

    private String formatDeleteIngredientesyNutrientes(String[] posData) {
        return "seek={\"numero\":\"" + posData[0] + "\"}&limit=1";
    }

    private String formatGetArtigos(String[] posData) {
        return "seek={\"codigo\":\"" + posData[0] + "\"}";
    }

}
