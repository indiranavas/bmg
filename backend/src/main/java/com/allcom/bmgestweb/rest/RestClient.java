package com.allcom.bmgestweb.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RestClient {

//    static Logger log = Logger.getLogger(RestClient.class);
    public static List<String> busquedaList = new ArrayList();
     public static List<List<String>> busquedaAcceso = new ArrayList();

    public static void RestClient(String tabla, String method, List<String> datainput, String ip, double size) throws ProtocolException, IOException {

        String tablaUse = null;
        String ind = null;

        if (tabla.equals("Articulos")) {
            tablaUse = "artigos";
            ind = "year";
        } else if (tabla.equals("Familias")) {
            tablaUse = "familias";
            ind = "year";
        } else if (tabla.equals("Acceso")) {
            tablaUse = "familias_art";
            ind = "year";
        } else if (tabla.equals("Ingredientes")) {
            tablaUse = "ingredientes";
            ind = "site";
        } else if (tabla.equals("Nutrientes")) {
            tablaUse = "nutrientes";
            ind = "site";
        } else if (tabla.equals("ExtraArticulos")) {
            tablaUse = "docs_dados_cab";
            ind = "year";
        }

//        try {
        if (method.equals("Mostrar")) {
            busquedaList = new ArrayList();
            if (tabla.equals("Acceso")) {
                busquedaAcceso = new ArrayList();
                GetRequestAcceso(getEndpointUrl(tablaUse, ip, ind), null, tabla);
            } else {
                GetRequest(getEndpointUrl(tablaUse, ip, ind), null, tabla);
            }
        } else if (method.equals("Insertar") || method.equals("Actualizar")) {
            if (datainput != null) {
//                StringBuffer linea = new StringBuffer();
//                int i = 0;
                Iterator<String> dataListIter = datainput.iterator();
                while (dataListIter.hasNext()) {

//                    linea = new StringBuffer();
                    String input = (String) dataListIter.next();

//                    linea.append(dataListIter.next()).append(",");
//                    dataListIter.remove();
                    PUTRequest(getEndpointUrl(tablaUse, ip, ind), input);
                }

            }
        } else if (method.equals("Eliminar")) {
            if (datainput != null) {
                Iterator<String> dataListIter = datainput.iterator();
                while (dataListIter.hasNext()) {
                    String input = (String) dataListIter.next();
                    DELETERequest(getEndpointUrl(tablaUse, ip, ind), input);
                }
            } else {
                for (int i = 0; i < size; i++) {
                    DELETERequest(getEndpointUrl(tablaUse, ip, ind), "");
                }

            }
        }
//        } catch (IOException ex) {
//            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, ex);
//        }

    }

    public static boolean getConection(String ip) {
        try {
            URL url = new URL(getEndpointUrl(null, ip, null));
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            conn.setConnectTimeout(8000);
            conn.connect();
            conn.disconnect();
            conn.getResponseCode();
            return true;
        } catch (MalformedURLException ex) {
//            log.error("La url no es correcta " + ex.getMessage());
            return false;
        } catch (IOException ex) {
//            log.error("No hay conexion con la balanza " + ex.getMessage());
            return false;
        }
    }

    private static List<String> GetRequest(String EndpointUrl, String input, String tabla) throws IOException {
        List<String> busqueda = new ArrayList();
        String inputNew = null;
        String bus = null;
        if (tabla.equals("Articulos") || tabla.equals("Familias")) {
            bus = "codigo";
        } else if (tabla.equals("Ingredientes") || tabla.equals("Nutrientes")) {
            bus = "numero";
        }
        busqueda = get(EndpointUrl, input, bus);
        for (int i = 0; i < busqueda.size(); i++) {
            if (!busquedaList.contains(busqueda.get(i).trim())) {
                busquedaList.add(busqueda.get(i).trim());
            }
        }
        if (busqueda.size() > 1) {
            if (tabla.equals("Articulos") || tabla.equals("Familias")) {
                inputNew = "?seek={\"codigo\":\"" + busqueda.get(busqueda.size() - 1).trim() + "\"}";
            } else if (tabla.equals("Ingredientes") || tabla.equals("Nutrientes")) {
                inputNew = "?seek={\"numero\":\"" + busqueda.get(busqueda.size() - 1).trim() + "\"}";
            }
            GetRequest(EndpointUrl, inputNew, tabla);
        }

        return busquedaList;
    }

    private static List<List<String>> GetRequestAcceso(String EndpointUrl, String input, String tabla) throws IOException {
        List<List<String>> busqueda = new ArrayList();
        String inputNew = null;
        String bus = "cod_cab";
        String bus1 = "linha_f";

        busqueda = getFamily(EndpointUrl, input, bus, bus1);
        for (int i = 0; i < busqueda.size(); i++) {
            for (int j = 0; j < busqueda.get(i).size(); j++) {
                if (!busquedaAcceso.contains(busqueda.get(i))) {
                    busquedaAcceso.add(busqueda.get(i));
                }
            }

        }
        if (busqueda.size() > 1) {

            inputNew = "?seek={\"cod_cab\":\"" + busqueda.get(busqueda.size() - 1).get(0).trim() + "\",\"linha_f\":\"" + busqueda.get(busqueda.size() - 1).get(1).trim() + "\"}";

            GetRequest(EndpointUrl, inputNew, tabla);
        }

        return busquedaAcceso;
    }

    private static List<String> get(String EndpointUrl, String input, String bus) throws ProtocolException, IOException {
        List<String> articles = new ArrayList();
        URL url;
//        try {
        if (input != null) {
            url = new URL(EndpointUrl + input);
        } else {
            url = new URL(EndpointUrl);
        }

        URLConnection uc = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) uc;

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        InputStreamReader in = new InputStreamReader(conn.getInputStream());
        String output;
        BufferedReader br = new BufferedReader(in);
        while ((output = br.readLine()) != null) {
            int index = output.indexOf(":");
            int indexbegin = output.indexOf("\"");
            if (index > 0 && indexbegin > 0) {
                if (output.substring(indexbegin + 1, index - 1).equals(bus)) {
                    articles.add(output.substring(index + 1, output.length() - 1).replace("\"", ""));
                }
            }
        }
        conn.disconnect();
//        } catch (MalformedURLException ex) {
//            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, ex);
//
//        }
//        System.out.println("request: " + bus + " " + articles.size());
        return articles;

    }

    private static List<List<String>> getFamily(String EndpointUrl, String input, String bus, String bus1) throws ProtocolException, IOException {
        List<List<String>> listFamilies = new ArrayList<List<String>>();
        List<String> families = new ArrayList();

//         List<String> articles = new ArrayList();
        URL url;
//        try {
        if (input != null) {
            url = new URL(EndpointUrl + input);
        } else {
            url = new URL(EndpointUrl);
        }

        URLConnection uc = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) uc;

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        InputStreamReader in = new InputStreamReader(conn.getInputStream());
        String output;
        BufferedReader br = new BufferedReader(in);
        while ((output = br.readLine()) != null) {
            int index = output.indexOf(":");
            int indexbegin = output.indexOf("\"");
            if (index > 0 && indexbegin > 0) {
                if (output.substring(indexbegin + 1, index - 1).equals(bus)) {
                    families.add(output.substring(index + 1, output.length() - 1).replace("\"", ""));
                } else if (output.substring(indexbegin + 1, index - 1).equals(bus1)) {
                    families.add(output.substring(index + 1, output.length() - 1).replace("\"", ""));
                }
               
            }
           if (families.size()==2) {
            listFamilies.add(families);
            families=new ArrayList();
        }

        }
        
        conn.disconnect();
//        } catch (MalformedURLException ex) {
//            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, ex);
//
//        }
//        System.out.println("request: " + bus + " " + articles.size());
        return listFamilies;

    }

    private static void PUTRequest(String EndpointUrl, String input) throws IOException {
        int code = 0;
        int index = input.indexOf("\\");
        if (index >= 0) {
            input = input.replace("\\", "");
        }

        index = input.indexOf("\t");
        if (index >= 0) {
            input = input.replace("\t", "");
        }

        try {

            URL url = new URL(EndpointUrl);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "application/json; charset=utf-8");

            conn.setRequestMethod("PUT");

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes("UTF-8"));
            os.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream()), "UTF-8"));
            code = conn.getResponseCode();

            conn.disconnect();

        } catch (Exception e) {
//            log.error("Error en input enviado a la balanza " + " " + input);
        }

//        return code;
    }

    private static void DELETERequest(String EndpointUrl, String input) throws IOException {
//        try {
        URL url;
        if (input.isEmpty()) {
            url = new URL(EndpointUrl);
        } else {
            url = new URL(EndpointUrl + "?" + input);
        }
//            URL url = new URL(EndpointUrl);
        URLConnection uc = url.openConnection();
        HttpURLConnection conn = (HttpURLConnection) uc;
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestProperty("Content-type", "application/json");
        conn.setRequestMethod("DELETE");

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (conn.getInputStream())));

        String output;

//            while ((output = br.readLine()) != null) { // aca si imprime un codigo
//                System.out.println(output);
//            }
        conn.disconnect();
//        } catch (Exception e) {
//            Logger.getLogger(RestClient.class.getName()).log(Level.SEVERE, null, e);
//        }

    }

    private static String getEndpointUrl(String tablaUse, String ip, String ind) {
        if (tablaUse != null && ind != null) {
            return "http://" + ip.trim() + ":7878/" + ind + "/" + tablaUse;
        } else {
            return "http://" + ip.trim() + ":7878";
        }

    }

//    private static HttpURLConnection getConn(String EndpointUrl) throws MalformedURLException, IOException {
//        URL url = new URL(EndpointUrl);
//        URLConnection uc = url.openConnection();
//        return (HttpURLConnection) uc;
//    }
}
