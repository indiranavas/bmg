package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Role;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer>{

    Role save(Role role);

//    void update(Role role);

    void delete(Role role);

//    List<Role> list();

    Role findByid(Integer id);
    
    Role findByname(String name);
    
    List<Role>findAll();
}
