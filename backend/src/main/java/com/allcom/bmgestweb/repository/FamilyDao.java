package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Family;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyDao extends JpaRepository<Family, String> {

   Family save(Family product);

    void deleteById(String id);

    List<Family> findAll();

    Family getById(String id);
}
