package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Scale;
import com.allcom.bmgestweb.domain.ScaleId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ScaleDao extends JpaRepository<Scale, ScaleId> {

//    Integer create(Scale scale);

//    void update(Scale scale);

    void delete(Scale scale);

//    Scale getById(ScaleId id);
    
   @Query("Select max(a.scaleId.id)+1 from Scale a where a.store.id=?1")
   String findNextId(String id);

}
