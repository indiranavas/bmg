package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Layout;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LayoutDao extends JpaRepository<Layout, String> {

   Layout save(Layout product);

    void deleteById(String id);

    List<Layout> findAll();

    Layout getById(String id);
}
