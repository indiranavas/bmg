package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Local;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreDao extends JpaRepository<Local, String>{

    Local save(Local store);

//    void update(Local store);
    
    void delete(Local store);
    
//    void deleteById(String id);
    
//    List<Store> list();
    List<Local> findAll();
    
    Local getById(String id);
}
