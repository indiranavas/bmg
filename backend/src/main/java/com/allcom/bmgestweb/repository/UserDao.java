package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

    User save(User user);
    
    User getById(int id);

    //void update(User user);
    void delete(User user);

    void deleteById(Integer id);

//    User getById(String id);
    List<User> findAll();

    Optional<User> findByuserLogin(String userLogin);

    Boolean existsByuserLogin(String userLogin);
//    Boolean existsByEmail(String email);

}
