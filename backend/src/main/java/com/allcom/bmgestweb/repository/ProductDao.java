package com.allcom.bmgestweb.repository;

import com.allcom.bmgestweb.domain.Product;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProductDao extends JpaRepository<Product, String>{

    Product save(Product product);

    void delete(Product product);

    List<Product> findAll();

    Product getById(String id);
}
