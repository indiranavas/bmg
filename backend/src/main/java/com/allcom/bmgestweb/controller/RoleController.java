package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domain.Role;
import com.allcom.bmgestweb.service.RoleServiceImpl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/rol"})
public class RoleController {

	@Autowired
	private RoleServiceImpl roleService;
	
	@GetMapping
	public List<Role> listar(){
		return roleService.findAll();
	}
        
        @GetMapping(path = {"/{id}"})
	public Role getById(@PathVariable("id") String id){
		return roleService.findById(Integer.parseInt(id));
	}

	@PostMapping
	public ResponseEntity<?> insertar(@RequestBody Role rol){
            if (roleService.findByName(rol.getName())!=null) {
                     return new ResponseEntity<>("El nombre del rol ya existe", HttpStatus.NOT_FOUND.OK);                
            }else{
                 return new ResponseEntity<>( roleService.save(rol), HttpStatus.OK);
            }
		
	}
	
	@PutMapping(path = {"/{id}"})
	public ResponseEntity<?> modificar(@RequestBody Role rol,@PathVariable("id") String id){
             rol.setId(Integer.valueOf(id));
//             if (roleService.findByName(rol.getName())!=null) {
//                     return new ResponseEntity<>("El nombre del rol ya existe", HttpStatus.NOT_FOUND.OK);                
//            }else{
                 return new ResponseEntity<>( roleService.save(rol), HttpStatus.OK);
//            }		
	}
	
	@DeleteMapping(path = {"/{id}"})
	public void eliminar(@PathVariable("id") Integer id) {
		roleService.delete(id);
	}
}
