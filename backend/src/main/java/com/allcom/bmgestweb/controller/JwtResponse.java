package com.allcom.bmgestweb.controller;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtResponse {
	private String token;
	private String type = "Bearer";
	private UserDetails UserDetails;
	private Collection<? extends GrantedAuthority> authorities;

	public JwtResponse(String accessToken, UserDetails UserDetails, Collection<? extends GrantedAuthority> authorities) {
		this.token = accessToken;
		this.UserDetails = UserDetails;
		this.authorities = authorities;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

    public UserDetails getUserDetails() {
        return UserDetails;
    }

    public void setUserDetails(UserDetails UserDetails) {
        this.UserDetails = UserDetails;
    }


	
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }
}