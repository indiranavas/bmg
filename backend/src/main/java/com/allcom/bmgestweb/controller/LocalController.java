package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domain.Local;
import com.allcom.bmgestweb.domainweb.LocalF;
import com.allcom.bmgestweb.domainweb.LocalForm;
import com.allcom.bmgestweb.service.StoreServiceImpl;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/store")
public class LocalController {

    @Autowired
    private StoreServiceImpl storeService;

    @GetMapping
    public List<LocalForm> listar() {
        return storeService.findAll();
    }

  @GetMapping(path = {"/{id}"})
    public LocalForm get(@PathVariable("id") String id) {
        return storeService.findById(id);
    }

    @PostMapping
    public  ResponseEntity<?> insertar(@Valid @RequestBody LocalForm sto) {
        if (storeService.findLocalById(sto.getLocal().getId())!=null) {
             return new ResponseEntity<>("El codigo de Local ya existe", HttpStatus.NOT_FOUND.OK); 
        }else{
            return new ResponseEntity<>(storeService.save(sto), HttpStatus.OK);
        }
        
    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<?> modificar(@RequestBody LocalForm store, @PathVariable("id") String id) {
        store.getLocal().setId(id);
        return new ResponseEntity<>(storeService.save(store), HttpStatus.OK);       
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> eliminar(@PathVariable("id") String id) {
        if (storeService.delete(id)!=null) {
            return new ResponseEntity<>(HttpStatus.OK); 
        }else{
             return new ResponseEntity<>("Error al eliminar el Local!", HttpStatus.NOT_FOUND.OK);
        } 
    }
    
 
}
