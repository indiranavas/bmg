/**
 * 
 */
package com.allcom.bmgestweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Swathi
 *
 */
@CrossOrigin(origins = "*")
@Controller
@RequestMapping("/api/auth")
public class HomeController {
	
	@GetMapping("/home")
	public String home() {
		return "forward:/index.html";
	}

}
