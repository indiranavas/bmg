package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domainweb.LayoutForm;
import com.allcom.bmgestweb.service.LayoutServiceImpl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/layout")
public class LayoutController {

    @Autowired
    private LayoutServiceImpl layoutService;

    @GetMapping
    public List<LayoutForm> listar() {
        return layoutService.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public LayoutForm get(@PathVariable("id") String id) {
        return layoutService.findById(id);
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody LayoutForm lay) {
        if (layoutService.findLayoutById(lay.getLayout().getId())!=null) {
           return new ResponseEntity<>("El codigo de Layout ya existe", HttpStatus.NOT_FOUND.OK); 
        }else{
           return new ResponseEntity<>(layoutService.save(lay), HttpStatus.OK); 
        }       
    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<?> modificar(@RequestBody LayoutForm lay, @PathVariable("id") String id) {
        lay.getLayout().setId(id);
         return new ResponseEntity<>(layoutService.save(lay), HttpStatus.OK);        
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> eliminar(@PathVariable("id") String id) {
        if (layoutService.deleteById(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }else{
           return new ResponseEntity<>("Error al eliminar el Layout!", HttpStatus.NOT_FOUND.OK); 
        }

    }   

}
