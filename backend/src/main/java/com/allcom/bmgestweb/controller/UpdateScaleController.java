package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domainweb.ScaleForm;
import com.allcom.bmgestweb.domainweb.UpdateLayoutForm;
import com.allcom.bmgestweb.rest.ScpConnect;
import com.allcom.bmgestweb.service.UpdateScaleServiceImpl;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/updateScale")
public class UpdateScaleController {

    @Autowired
    private UpdateScaleServiceImpl updateScaleService;

    @PutMapping
    public List<ScaleForm> get(@RequestBody List<ScaleForm> scales) {
        return updateScaleService.getStatusScales(scales);
    }
    
    @PutMapping(path = {"/{id}"})
    public void sendImage(@PathVariable("id") String id){
        FileInputStream in = null;
        try {
            ScpConnect scpConnect = new ScpConnect("192.168.10.160", "root","");
            File fileIn = new File("C:\\Users\\Vanessa\\Desktop\\productos\\01057.jpg");
            in = new FileInputStream(fileIn);
            scpConnect.sendFile(in,fileIn.length(),"plu"+id+".bmp", "/local/data/img/productos");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UpdateScaleController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ConnectException ex) {
            Logger.getLogger(UpdateScaleController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(UpdateScaleController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
   
    @PostMapping
    public void restConnect(@Valid @RequestBody UpdateLayoutForm updateLayout){
        updateScaleService.restConnect(updateLayout);
    }
}
