package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.service.ScaleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/scale")
public class ScaleController {

    @Autowired
    private ScaleServiceImpl scaleService;

//    @DeleteMapping(path = {"/{id}"})
//    public void eliminar(@PathVariable("id") int id) {
//        scaleService.deleteScales(id);
//    }
    
   @GetMapping(path = {"/{id}"})
    public int get(@PathVariable("id") String id) {
        return scaleService.findNextId(id);
    }
}
