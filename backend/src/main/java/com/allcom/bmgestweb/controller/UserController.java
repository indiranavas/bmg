package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domainweb.UserF;
import com.allcom.bmgestweb.domainweb.UserForm;
import com.allcom.bmgestweb.service.UserServiceImpl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @GetMapping
    public List<UserForm> listar() {
        return userService.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public UserF get(@PathVariable("id") int id) {
        return userService.findById(id);
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody UserF user) {
        if (userService.existsByuserLogin(user.getUserName())) {
            return new ResponseEntity<>("El nombre de usuario ya existe", HttpStatus.NOT_FOUND.OK);
        } else {
            return new ResponseEntity<>(userService.save(user), HttpStatus.OK);
        }

    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<?> modificar(@RequestBody UserF user, @PathVariable("id") int id) {
        user.setId(id);
//        if (userService.existsByuserLogin(user.getUserName())) {
//            return new ResponseEntity<>("El nombre de usuario ya existe", HttpStatus.NOT_FOUND.OK);
//        } else {
            return new ResponseEntity<>(userService.update(user), HttpStatus.OK);
//        }
    }

    @DeleteMapping(value = "/{id}")
    public void eliminar(@PathVariable("id") Integer id) {
        userService.deleteById(id);
    }

}
