package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domainweb.ProductForm;
import com.allcom.bmgestweb.service.ProductService;
import java.util.List;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/product"})
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(path = {"/{id}"})
    public Product get(@PathVariable("id") String id) {
        return productService.findById(id);
    }

    @GetMapping
    public List<ProductForm> listar() {
        return productService.findAll();
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody Product product) {
        if (productService.findById(product.getId()) != null) {
            return new ResponseEntity<>("El codigo de producto ya existe", HttpStatus.NOT_FOUND.OK);
        } else {           
            return new ResponseEntity<>(productService.save(product), HttpStatus.OK);
        }
    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<?> modificar(@RequestBody Product product, @PathVariable("id") String id) {
        product.setId(id);
        return new ResponseEntity<>(productService.save(product), HttpStatus.OK);
    }

    @DeleteMapping(path = {"/{id}"})
    public ResponseEntity<?> eliminar(@PathVariable("id") String id) {
        if (productService.delete(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>("El Producto esta asociado a una Familia", HttpStatus.NOT_FOUND.OK);
        }

    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        File convertFile = new File("assets\\images\\producto\\" + file.getOriginalFilename());
        convertFile.createNewFile();
        FileOutputStream fout = new FileOutputStream(convertFile);
        fout.write(file.getBytes());
        fout.close();
        return new ResponseEntity<>("File is uploaded successfully", HttpStatus.OK);
    }

}
