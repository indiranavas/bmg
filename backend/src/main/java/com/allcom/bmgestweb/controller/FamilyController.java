package com.allcom.bmgestweb.controller;

import com.allcom.bmgestweb.domainweb.FamilyF;
import com.allcom.bmgestweb.domainweb.FamilyForm;
import com.allcom.bmgestweb.service.FamilyServiceImpl;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/family")
public class FamilyController {

    @Autowired
    private FamilyServiceImpl familyService;

    @GetMapping
    public List<FamilyForm> listar() {
        return familyService.findAll();
    }

    @GetMapping(path = {"/{id}"})
    public FamilyForm get(@PathVariable("id") String id) {
        return familyService.findById(id);
    }

    @PostMapping
    public ResponseEntity<?> insertar(@RequestBody FamilyForm fam) {
        if (familyService.findFamilyById(fam.getFamily().getId()) != null) {
            return new ResponseEntity<>("El codigo de familia ya existe", HttpStatus.NOT_FOUND.OK);
        } else {
            return new ResponseEntity<>(familyService.save(fam), HttpStatus.OK);
        }

    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<?> modificar(@RequestBody FamilyForm fam, @PathVariable("id") String id) {
        fam.getFamily().setId(id);
        return new ResponseEntity<>(familyService.save(fam), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> eliminar(@PathVariable("id") String id) {
        if (familyService.deleteById(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>("La familia esta asociada a un layout", HttpStatus.NOT_FOUND.OK);
        }

    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file,@RequestParam("url") String url) {
        
         String Url=url +"/bmgestR/img/" + file.getOriginalFilename();
          System.out.println(Url);
         String urlnew= Url.replace("/", "\\\\");
         System.out.println("URL NEW " + urlnew);
        try {
            File convertFile = new File("http:\\\\localhost:8080\\img\\"+file.getOriginalFilename());
            System.out.println(convertFile);
            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(file.getBytes());
            fout.close();
            return new ResponseEntity<>("File is uploaded successfully", HttpStatus.OK);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(FamilyController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

}
