package com.allcom.bmgestweb.domainweb;

import com.allcom.bmgestweb.domain.Family;
import java.util.List;

public class UpdateLayoutForm {

    List<FamilyForm> families;

    List<ScaleForm> scales;

    public List<FamilyForm> getFamilies() {
        return families;
    }

    public void setFamilies(List<FamilyForm> families) {
        this.families = families;
    }

    public List<ScaleForm> getScales() {
        return scales;
    }

    public void setScales(List<ScaleForm> scales) {
        this.scales = scales;
    }

   
}
