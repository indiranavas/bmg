
package com.allcom.bmgestweb.domainweb;

import javax.validation.constraints.NotNull;

public class LocalF {
     
  
   private String id;

   private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }  
   
}
