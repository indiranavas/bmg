
package com.allcom.bmgestweb.domainweb;

import java.util.List;

public class LocalForm {

    LocalF local;
    List<ScaleForm> scales;

    public LocalF getLocal() {
        return local;
    }

    public void setLocal(LocalF local) {
        this.local = local;
    }

    public List<ScaleForm> getScales() {
        return scales;
    }

    public void setScales(List<ScaleForm> scales) {
        this.scales = scales;
    }
   
}
