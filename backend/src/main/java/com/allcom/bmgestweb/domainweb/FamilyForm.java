
package com.allcom.bmgestweb.domainweb;

import com.allcom.bmgestweb.domain.Product;
import java.util.List;

public class FamilyForm {

    public FamilyF getFamily() {
        return family;
    }

    public void setFamily(FamilyF family) {
        this.family = family;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    FamilyF family;
    List<Product> products;
}
