
package com.allcom.bmgestweb.domainweb;

import com.allcom.bmgestweb.domain.Role;

public class UserForm {
    UserF userF = new UserF();
    Role rol = new Role();

    public UserF getUserF() {
        return userF;
    }

    public void setUserF(UserF userF) {
        this.userF = userF;
    }

    public Role getRol() {
        return rol;
    }

    public void setRol(Role rol) {
        this.rol = rol;
    }
}
