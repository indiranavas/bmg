
package com.allcom.bmgestweb.domainweb;

import com.allcom.bmgestweb.domain.Family;
import java.util.List;

public class LayoutForm {

    LayoutF layout;
    List<FamilyForm> families;

    public LayoutF getLayout() {
        return layout;
    }

    public void setLayout(LayoutF layout) {
        this.layout = layout;
    }

    public List<FamilyForm> getFamilies() {
        return families;
    }

    public void setFamilies(List<FamilyForm> families) {
        this.families = families;
    }
       
}
