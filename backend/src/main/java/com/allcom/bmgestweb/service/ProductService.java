
package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domainweb.ProductForm;
import java.util.List;

public interface ProductService {
    Product save(Product product);
    List<ProductForm> findAll();
    Product findById(String id);
    boolean delete(String id);
}
