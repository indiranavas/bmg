package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domainweb.ProductForm;
import com.allcom.bmgestweb.repository.ProductDao;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("productService")
public class ProductServiceImpl implements ProductService{

    @Autowired
    ProductDao productRepository;

    @Override
    public Product save(Product product) {
//        product.setImage("C:\\BmgestWeb\\Imagenes\\Productos\\" + product.getImage());
       return productRepository.save(product);
    }

    @Override
    public List<ProductForm> findAll() {
        List<ProductForm> productsForm = new ArrayList();
        List<Product> product = productRepository.findAll();
        for (int i = 0; i < product.size(); i++) {
            ProductForm productForm = new ProductForm();
            productForm.setId(product.get(i).getId());
            productForm.setDescription(product.get(i).getDescription());
            productForm.setImage(product.get(i).getImage().replace("\\", "/"));
            productsForm.add(productForm);
        }
        return productsForm;
    }

    @Override
    public Product findById(String id) {
        Product product = productRepository.getById(id);
//        product.setImage(product.getImage().replace("\\", "/"));
        return product;
    }

    @Override
    public boolean delete(String id) {
        Product product = productRepository.getById(id);
        boolean delete= false;
       if (product != null) {
           if (product.getListFamily().isEmpty()) {         
             productRepository.delete(product); 
             delete= true;
           }  
           
        }
       return delete;
    }

}
