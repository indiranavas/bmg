
package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domain.Role;
import com.allcom.bmgestweb.repository.RoleDao;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("roleService")
public class RoleServiceImpl {
    
    @Autowired
    RoleDao roleRepository;
    
    public Role findByName(String nameRol){
        return roleRepository.findByname( nameRol);
    }
    
    public List<Role>findAll(){
       return roleRepository.findAll();
    }
    
    public Role findById(Integer id){
        return roleRepository.findByid(id);
    }
    
    public Role findByname(String name){
        return roleRepository.findByname(name);
    }
    
    public Role save(Role rol){
       return roleRepository.save(rol);
    }
    
    public Role delete(int id){
        
       Role role = roleRepository.findByid(id);
        if (role != null) {
            roleRepository.delete(role);
        }
        return role;
    }
}
