package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.controller.UpdateScaleController;
import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domainweb.FamilyForm;
import com.allcom.bmgestweb.domainweb.ScaleForm;
import com.allcom.bmgestweb.domainweb.UpdateLayoutForm;
import com.allcom.bmgestweb.rest.FormatLine;
import com.allcom.bmgestweb.rest.RestClient;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;

@Service("updateScaleService")
public class UpdateScaleServiceImpl {

    public List<ScaleForm> getStatusScales(List<ScaleForm> scales) {
        List<ScaleForm> scalesNew = new ArrayList();
        for (int i = 0; i < scales.size(); i++) {
            ScaleForm scaleForm = new ScaleForm();
            scaleForm = scales.get(i);
            scaleForm.setStatus(RestClient.getConection(scaleForm.getIp()));
            scalesNew.add(scaleForm);
        }
        return scalesNew;
    }

    public void restConnect(UpdateLayoutForm updateLayout) {
        try {
            List<ScaleForm> listScales = updateLayout.getScales();
            List<String> dataList = new ArrayList();
            List<String> deleteAccess = new ArrayList();
            FormatLine formatLine = new FormatLine();
            for (int i = 0; i < listScales.size(); i++) {
                boolean rein = RestClient.getConection(listScales.get(i).getIp());
                if (rein) {
                    RestClient.RestClient("Acceso", "Mostrar", null, listScales.get(i).getIp(), 0);
                    RestClient.RestClient("Familias", "Mostrar", null, listScales.get(i).getIp(), 0);
                    List<List<String>> accesos = RestClient.busquedaAcceso;
                    List<String> familias = RestClient.busquedaList;
                    List<FamilyForm> listFamily = updateLayout.getFamilies();
                    List<String> res = new ArrayList();
                    for (int k = 0; k < listFamily.size(); k++) {
                        res.add(listFamily.get(i).getFamily().getId());
                    }
                    for (int j = 0; j < listFamily.size(); j++) {
//                        if (familias.contains(listFamily.get(j).getFamily().getId())) {
//                            List<Product> listProduct = listFamily.get(j).getProducts();
//                            int position = 0;
//                            for (int k = 0; k < listProduct.size(); k++) {
//                                position++;
//                                String articleId = listProduct.get(k).getId();
//                                String idArticle = articleId.format("%5s", articleId).replace(' ', '0');
//                                String groupId = listFamily.get(j).getFamily().getId();
//                                String idfamily = groupId.format("%2s", groupId).replace(' ', '0');
//                                String line = new String();
//                                line = line.concat(idArticle).concat(",").concat(idfamily).concat(",").concat(String.valueOf(position));
//                                dataList.add(line);
//                            }
//                            for (int k = 0; k < accesos.size(); k++) {
//                                if (familias.equals(accesos.get(k).get(0))) {
//                                    String line = new String();
//                                    line = line.concat(accesos.get(k).get(0)).concat(",").concat(accesos.get(k).get(1));
//                                    deleteAccess.add(line);
//                                }
//                            }
//                            List<String> dataFormat = formatLine.format(deleteAccess, "Acceso", "Eliminar");
//                            RestClient.RestClient("Acceso", "Eliminar", dataFormat, listScales.get(i).getIp(), 0);
//                            dataFormat = formatLine.format(dataList, "Acceso", "Insertar");
//                            RestClient.RestClient("Acceso", "Insertar", dataFormat, listScales.get(i).getIp(), 0);
//                            dataList = new ArrayList();
//                            deleteAccess = new ArrayList();
//                        } else {
                        String line = new String();
                        Base64.Encoder encoder = Base64.getEncoder();
                                String b = encoder.encodeToString(" ".getBytes(StandardCharsets.UTF_8));
                        line = line.concat(listFamily.get(j).getFamily().getId()).concat(",").concat(listFamily.get(j).getFamily().getDescription())
                                .concat(",").concat("familias/" + listFamily.get(j).getFamily().getId() + ".jpg").concat(",").concat("0")
                                .concat(",").concat(b)
                                .concat(",").concat("1").concat(",").concat("1");
                        dataList.add(line);
                        List<String> dataFormat = formatLine.format(dataList, "Familias", "Insertar");
                        RestClient.RestClient("Familias", "Insertar", dataFormat, listScales.get(i).getIp(), 0);
                        List<Product> listProduct = listFamily.get(j).getProducts();
                        int position = 0;
                        dataList = new ArrayList();
                        for (int k = 0; k < listProduct.size(); k++) {
                            position++;
                            String articleId = listProduct.get(k).getId();
                            String idArticle = articleId.format("%5s", articleId).replace(' ', '0');
                            String groupId = listFamily.get(j).getFamily().getId();
                            String idfamily = groupId.format("%2s", groupId).replace(' ', '0');
                            line = new String();
                            line = line.concat(idArticle).concat(",").concat(idfamily).concat(",").concat(String.valueOf(position));
                            dataList.add(line);
                        }
                        dataFormat = formatLine.format(dataList, "Acceso", "Insertar");
                        RestClient.RestClient("Acceso", "Insertar", dataFormat, listScales.get(i).getIp(), 0);
                        dataList = new ArrayList();
//                            List<String> res = new ArrayList();
//                            for (int k = 0; k < listFamily.size(); k++) {
//                                res.add(listFamily.get(i).getFamily().getId());
//                            }
//                            res.retainAll(familias);
//                        }

                    }
                    for (int k = 0; k < familias.size(); k++) {
                        for (int j = 0; j < res.size(); j++) {
                            if (!res.get(j).equals(familias.get(k))) {
                                Base64.Encoder encoder = Base64.getEncoder();
                                String b = encoder.encodeToString("?".getBytes(StandardCharsets.UTF_8));
                                String line = new String();
                                line = line.concat(familias.get(k)).concat(",").concat(b);
                                dataList.add(line);
                            }
                        }
                    }
                    List<String> dataFormat = formatLine.format(dataList, "Familias", "Actualizar");
                    RestClient.RestClient("Familias", "Actualizar", dataFormat, listScales.get(i).getIp(), 0);

                }
            }

//            List<FamilyForm> listFamily = updateLayout.getFamilies();
//            int position = 0;
//            for (int i = 0; i < listFamily.size(); i++) {
//                List<Product> listProduct = listFamily.get(i).getProducts();
//                for (int j = 0; j < listProduct.size(); j++) {
//                    position++;
//                    String articleId = listProduct.get(j).getId();
//                    String idArticle = articleId.format("%5s", articleId).replace(' ', '0');
//                    String groupId = listFamily.get(i).getFamily().getId();
//                    String idfamily = groupId.format("%2s", groupId).replace(' ', '0');
//                    String line = new String();
//                    line = line.concat(idArticle).concat(",").concat(idfamily).concat(",").concat(String.valueOf(position));
//                    dataList.add(line);
//                }
//            }
//
//            List<String> dataFormat = formatLine.format(dataList, "Acceso", "Insertar");
//            for (int i = 0; i < listScales.size(); i++) {
//                boolean rein = RestClient.getConection(listScales.get(i).getIp());
//                if (rein) {
//                    RestClient.RestClient("Articulos", "Mostrar", null, listScales.get(i).getIp(), 0);
//                    double cant = (double) (RestClient.busquedaList.size()) / (double) (100);
//                    RestClient.RestClient("Acceso", "Eliminar", null, listScales.get(i).getIp(), Math.round(cant));
//                    RestClient.RestClient("Acceso", "Insertar", dataFormat, listScales.get(i).getIp(), 0);
//                }
//            }
        } catch (IOException ex) {
            Logger.getLogger(UpdateScaleController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
