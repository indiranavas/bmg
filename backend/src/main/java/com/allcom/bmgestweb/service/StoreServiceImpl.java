package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Scale;
import com.allcom.bmgestweb.domain.Local;
import com.allcom.bmgestweb.domain.ScaleId;
import com.allcom.bmgestweb.domainweb.LocalF;
import com.allcom.bmgestweb.domainweb.LocalForm;
import com.allcom.bmgestweb.domainweb.ScaleForm;
import com.allcom.bmgestweb.repository.StoreDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("storeService")
public class StoreServiceImpl {

    @Autowired
    StoreDao storeRepository;

    public List<LocalForm> findAll() {
        List<LocalForm> storesForm = new ArrayList();
        List<Local> stores = storeRepository.findAll();
        for (int i = 0; i < stores.size(); i++) {
            Local store = stores.get(i);
            LocalF storeF = new LocalF();
            LocalForm storeForm = new LocalForm();
            List<ScaleForm> scaleList = new ArrayList();
            storeF.setId(store.getId());
            storeF.setDescription(store.getDescription());
            for (int j = 0; j < store.getListScale().size(); j++) {
                ScaleForm scaleForm = new ScaleForm();
                scaleForm.setId(store.getListScale().get(j).getScaleId().getId());
                scaleForm.setDescription(store.getListScale().get(j).getDescription());
                scaleForm.setIp(store.getListScale().get(j).getIp());
                scaleList.add(scaleForm);
            }
            storeForm.setLocal(storeF);
            storeForm.setScales(scaleList);
            storesForm.add(storeForm);
        }
        return storesForm;
//        List<LocalF> storesForm = new ArrayList();
//        List<Local> store = storeRepository.findAll();
//        for (int i = 0; i < store.size(); i++) {
//            LocalF storeForm = new LocalF();
//            storeForm.setId(store.get(i).getId());
//            storeForm.setDescription(store.get(i).getDescription());
//            storesForm.add(storeForm);
//        }
//        return storesForm;
    }

    public Local save(LocalForm storeForm) {
        Local store = new Local();
        List<Scale> scales = new ArrayList();

        store.setId(storeForm.getLocal().getId());
        store.setDescription(storeForm.getLocal().getDescription());
        for (int i = 0; i < storeForm.getScales().size(); i++) {
            Scale scale = new Scale();
            ScaleId scaleId = new ScaleId();
            scaleId.setId(storeForm.getScales().get(i).getId());
            scaleId.setIdStore(store.getId());
            scale.setScaleId(scaleId);
            scale.setDescription(storeForm.getScales().get(i).getDescription());
            scale.setIp(storeForm.getScales().get(i).getIp());
            scale.setStore(store);
            scales.add(scale);
        }
        store.setListScale(scales);
        return storeRepository.save(store);
    }

    public LocalForm findById(String id) {
        LocalForm storeForm = new LocalForm();
        List<ScaleForm> scaleList = new ArrayList();
        Local store = storeRepository.getById(id);
        LocalF storeF = new LocalF();
        storeF.setId(store.getId());
        storeF.setDescription(store.getDescription());
        for (int i = 0; i < store.getListScale().size(); i++) {
            ScaleForm scaleForm = new ScaleForm();
            scaleForm.setId(store.getListScale().get(i).getScaleId().getId());
            scaleForm.setDescription(store.getListScale().get(i).getDescription());
            scaleForm.setIp(store.getListScale().get(i).getIp());
            scaleList.add(scaleForm);
        }
        storeForm.setLocal(storeF);
        storeForm.setScales(scaleList);
        return storeForm;
    }
    
      public Local findLocalById(String id) {
       return storeRepository.getById(id);
    }

    public void deleteById(String id) {
        storeRepository.deleteById(id);
    }

    public LocalForm delete(String id) {
        Local local = storeRepository.getById(id);
        LocalForm localForm = new LocalForm();
        LocalF localf = new LocalF();
        localf.setId(local.getId());
        localf.setDescription(local.getDescription());
        localForm.setLocal(localf);
         List<ScaleForm> listScales = new ArrayList();
        for (int i = 0; i < local.getListScale().size(); i++) {
           ScaleForm scale = new ScaleForm();
           scale.setId(local.getListScale().get(i).getScaleId().getId());
           scale.setDescription(local.getListScale().get(i).getDescription());
           scale.setIp(local.getListScale().get(i).getIp());
           listScales.add(scale);
        }
        localForm.setScales(listScales);
        if (local != null) {
            storeRepository.delete(local);
        }
        return localForm;
    }

}
