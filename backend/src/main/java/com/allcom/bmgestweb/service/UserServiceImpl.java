package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Role;
import com.allcom.bmgestweb.domain.User;
import com.allcom.bmgestweb.domainweb.UserF;
import com.allcom.bmgestweb.domainweb.UserForm;
import com.allcom.bmgestweb.repository.UserDao;
import com.allcom.bmgestweb.security.jwt.UserPrinciple;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserDetailsService {

    @Autowired
    UserDao userRepository;

    @Autowired
    private RoleServiceImpl roleService;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByuserLogin(username).orElseThrow(
                () -> new UsernameNotFoundException("User Not Found with -> username or email : " + username));
        return UserPrinciple.build(user);

    }

    public UserF findById(int id) {
        User user = userRepository.getById(id);
        UserF userForm = new UserF();
        userForm.setId(user.getId());
        userForm.setName(user.getName());
        userForm.setLastName(user.getLastName());
        userForm.setRolId(user.getRole().getId());
        userForm.setPassword(user.getPassword());
        userForm.setUserName(user.getUserLogin());
        userForm.setStatus(user.isStatus());
        return userForm;
    }

    public List<UserForm> findAll() {
        List<User> users = userRepository.findAll();
        List<UserForm> usersForm = new ArrayList();
        for (int i = 0; i < users.size(); i++) {
            User user = users.get(i);
            UserF userF = new UserF();
            UserForm userForm = new UserForm();
            userF.setId(user.getId());
            userF.setName(user.getName());
            userF.setLastName(user.getLastName());
            userF.setRolId(user.getRole().getId());
            userF.setPassword(user.getPassword());
            userF.setStatus(user.isStatus());
            userF.setUserName(user.getUserLogin());
            userForm.setUserF(userF);
            userForm.setRol(user.getRole());
            usersForm.add(userForm);
        }
        return usersForm;
    }

    public boolean existsByuserLogin(String username) {
        return userRepository.existsByuserLogin(username);
    }

    public User save(UserF userForm) {
        User user = new User(userForm.getName(), userForm.getLastName(), userForm.getUserName(),
                encoder.encode(userForm.getPassword()));
        Role rol = roleService.findById(userForm.getRolId());
        user.setRole(rol);
        return userRepository.save(user);

    }

    public User update(UserF userForm) {
        User user = new User();
        user.setId(userForm.getId());
        user.setName(userForm.getName());
        user.setLastName(userForm.getLastName());
        user.setUserLogin(userForm.getUserName());
        user.setStatus(userForm.isStatus());
        if (userForm.getNewPassword()!=null) {
             user.setPassword(encoder.encode(userForm.getNewPassword()));
        }else{
            user.setPassword(userForm.getPassword());
        }
        
        Role rol = roleService.findById(userForm.getRolId());
        user.setRole(rol);
        return userRepository.save(user);

    }

    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

}
