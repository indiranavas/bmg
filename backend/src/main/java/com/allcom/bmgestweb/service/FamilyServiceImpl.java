package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Family;
import com.allcom.bmgestweb.domain.Product;
import com.allcom.bmgestweb.domainweb.FamilyF;
import com.allcom.bmgestweb.domainweb.FamilyForm;
import com.allcom.bmgestweb.repository.FamilyDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("familyService")
public class FamilyServiceImpl {

    @Autowired
    FamilyDao familyRepository;

    public Family save(FamilyForm familyForm) {
        Family family = new Family();
        family.setId(familyForm.getFamily().getId());
        family.setDescription(familyForm.getFamily().getDescription());
        family.setImage(familyForm.getFamily().getImage());
        family.setListProduct(familyForm.getProducts());
       return familyRepository.save(family);
    }

    public List<FamilyForm> findAll() {
        List<FamilyForm> familiesForm = new ArrayList();
        List<Family> product = familyRepository.findAll();
        for (int i = 0; i < product.size(); i++) {
            FamilyForm familyForm = new FamilyForm();
            FamilyF familiF = new FamilyF();
            familiF.setId(product.get(i).getId());
            familiF.setDescription(product.get(i).getDescription());
            familiF.setImage(product.get(i).getImage().replace("\\", "/"));
            familyForm.setFamily(familiF);
            familyForm.setProducts(product.get(i).getListProduct());
            familiesForm.add(familyForm);
        }
        return familiesForm;
      
    }

    public FamilyForm findById(String id) {
        FamilyForm familyForm = new FamilyForm();
        Family family= familyRepository.getById(id);
        FamilyF familyF= new FamilyF();
        familyF.setId(family.getId());
        familyF.setDescription(family.getDescription());
        familyF.setImage(family.getImage());
        List<Product> productList = family.getListProduct();
        familyForm.setFamily(familyF);
        familyForm.setProducts(productList);
        return familyForm;
    }
    
    public Family findFamilyById(String id) {
       return familyRepository.getById(id);
    }

    public boolean deleteById(String id) {
        Family family=familyRepository.getById(id);
        boolean delete=false;
        if (family!=null) {
            if (family.getListLayout().isEmpty()) {
               familyRepository.deleteById(id);
               delete=true;
            }
        }
       return delete; 
    }
}
