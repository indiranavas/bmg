package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Family;
import com.allcom.bmgestweb.domain.Layout;
import com.allcom.bmgestweb.domainweb.FamilyF;
import com.allcom.bmgestweb.domainweb.FamilyForm;
import com.allcom.bmgestweb.domainweb.LayoutF;
import com.allcom.bmgestweb.domainweb.LayoutForm;
import com.allcom.bmgestweb.repository.LayoutDao;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("layoutService")
public class LayoutServiceImpl {

    @Autowired
    LayoutDao layoutRepository;

    public Layout save(LayoutForm layoutForm) {
        Layout layout = new Layout();
        layout.setId(layoutForm.getLayout().getId());
        layout.setName(layoutForm.getLayout().getName());
        List<Family> familyList = new ArrayList();
        for (int i = 0; i < layoutForm.getFamilies().size(); i++) {
            Family family = new Family();
            family.setId(layoutForm.getFamilies().get(i).getFamily().getId());
            family.setDescription(layoutForm.getFamilies().get(i).getFamily().getDescription());
            family.setImage(layoutForm.getFamilies().get(i).getFamily().getImage());
            family.setListProduct(layoutForm.getFamilies().get(i).getProducts());
            familyList.add(family);
        }
        layout.setListFamily(familyList);
        return layoutRepository.save(layout);
    }

    public List<LayoutForm> findAll() {
        List<LayoutForm> layoutsForm = new ArrayList();
        List<Layout> layouts = layoutRepository.findAll();
         List<FamilyForm> familyList = new ArrayList();
        for (int i = 0; i < layouts.size(); i++) {
           familyList = new ArrayList();
            LayoutF layoutF = new LayoutF();
            LayoutForm layoutForm = new LayoutForm();
            layoutF.setId(layouts.get(i).getId());
            layoutF.setName(layouts.get(i).getName());
            for (int j = 0; j < layouts.get(i).getListFamily().size(); j++) {
                FamilyF familyF = new FamilyF();
                familyF.setId(layouts.get(i).getListFamily().get(j).getId());
                familyF.setDescription(layouts.get(i).getListFamily().get(j).getDescription());
                familyF.setImage(layouts.get(i).getListFamily().get(j).getImage());                
                FamilyForm familyForm = new FamilyForm();
                familyForm.setFamily(familyF);
                familyForm.setProducts(layouts.get(i).getListFamily().get(j).getListProduct());
                familyList.add(familyForm);
            }          
            layoutForm.setLayout(layoutF);
            layoutForm.setFamilies(familyList);
            layoutsForm.add(layoutForm);
        }
        return layoutsForm;

    }

    public LayoutForm findById(String id) {
        LayoutForm layoutForm = new LayoutForm();
        Layout layout = layoutRepository.getById(id);
        LayoutF LayoutF = new LayoutF();
        LayoutF.setId(layout.getId());
        LayoutF.setName(layout.getName());
        List<FamilyForm> familyList = new ArrayList();
        for (int j = 0; j < layout.getListFamily().size(); j++) {
                FamilyF familyF = new FamilyF();
                familyF.setId(layout.getListFamily().get(j).getId());
                familyF.setDescription(layout.getListFamily().get(j).getDescription());
                familyF.setImage(layout.getListFamily().get(j).getImage());                
                FamilyForm familyForm = new FamilyForm();
                familyForm.setFamily(familyF);
                familyForm.setProducts(layout.getListFamily().get(j).getListProduct());
                familyList.add(familyForm);
            }  
        layoutForm.setLayout(LayoutF);
        layoutForm.setFamilies(familyList);
        return layoutForm;
    }
    
     public Layout findLayoutById(String id) {
       return layoutRepository.getById(id);
    }

    public boolean deleteById(String id) {
        boolean delete;
        try {
            layoutRepository.deleteById(id);
            delete=true;
        } catch (Exception e) {
            delete=false;
        }
        return delete;
    }
}
