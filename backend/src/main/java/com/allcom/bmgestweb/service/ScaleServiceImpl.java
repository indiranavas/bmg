
package com.allcom.bmgestweb.service;

import com.allcom.bmgestweb.domain.Scale;
import com.allcom.bmgestweb.repository.ScaleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("scaleService")
public class ScaleServiceImpl {
    
     @Autowired
    ScaleDao scaleRepository;
     
//    public void deleteScales(int id) {
//        Scale scale = scaleRepository.getById(id);
//        if (scale != null) {
//            scaleRepository.delete(scale);
//        }
//    }
    
   public int findNextId(String id){
       String idScale=scaleRepository.findNextId(id);
       if (idScale!=null) {
           return Integer.valueOf(idScale);
       }else{
           return 1;
       }
   }
}
