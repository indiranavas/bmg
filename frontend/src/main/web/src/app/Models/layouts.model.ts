// Model layout

import {Family} from "./family.model";

export class Layout {
  id: number;
  name: string;
  families: Family[];

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
  
}