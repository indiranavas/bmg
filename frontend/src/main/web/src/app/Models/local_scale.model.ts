// Model local_scale
export class Local_Scale {
  id: number;
  active: boolean;
  code: string;
  description:string
  ip: string;
  port: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
} 