// Model scale
export class Scale {
  id:number;
  code:string;
  description:string;
  ip:string;
  status:boolean;
 
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
  
}