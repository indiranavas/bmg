// Model users
import {Rol} from "./rol.model";

export class Users {
  id:number;
  name:string;
  lastName:string;
  userName:string;
  password:string;
  newPassword:string; 
  status:boolean;  
  rolId:number;
  rol:Rol;
  
 
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
 
  