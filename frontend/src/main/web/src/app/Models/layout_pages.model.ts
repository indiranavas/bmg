// Model layoutpages
export class LayoutPages {
  layout_id: number;
  pages_id: number;
 
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}