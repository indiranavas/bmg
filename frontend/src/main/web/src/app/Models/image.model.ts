// Model image
export class Image {
  id: number;
  code: string;
  description: string;
  type:number;
  family: number;
  product: number;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}