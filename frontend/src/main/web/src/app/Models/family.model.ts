// Model family
export class Family {
  id: string;  
  description: string;
  image: string;
  checked: boolean;
  products : String[]; 
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}