// Model history
export class History {
  id:number;
  active:boolean;
  code: string;
  type:string;
  object:string;
  name:string;
  layout:string;
  locals:string;
  date:number;
  iscreation:boolean;
  isdelete:boolean;
 

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}
 
  