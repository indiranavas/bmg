// Model pages_images
export class Pages_Images {
  page_id: number;
  images_id: number;
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}