// Model rol
export class Rol {
id: number;
name: string;
description:string;
accessStatistics:number;
accessStore:number;
accessLayout: number;
accessUpdateScale: number;
accessProduct:number;
accessFamily:number;
accessAdministration:number;
accessActivity:number;

constructor(values: Object = {}) {
    Object.assign(this, values);
  }

}