// Model familylayout
export class FamilyLayout {
  id: number;
  active: boolean;
  code: string;
  description: string;
  imgExist: boolean;
  imgExtension: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}