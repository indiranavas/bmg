// Model familylayoutproduct
export class Familylayoutproduct {
  family_layout_id: number;
  product_id: number;
  description: string;
  imgExist: boolean;
  imgExtension: string;
  name: string;
  code: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
  
}