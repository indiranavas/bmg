// Model local
import {Scale} from "./scale.model";


export class Local {
  id: string; 
  description: string;
  scales: Scale[];
  
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
  
}

