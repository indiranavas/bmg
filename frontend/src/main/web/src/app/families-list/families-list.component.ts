import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;

import { FamilyServiceService } from '../Service/family/family-service.service';
import { Family } from '../Models/family.model';
import { TokenStorageService } from '../auth/token-storage.service';


@Component({
  selector: 'app-families-list',
  templateUrl: './families-list.component.html',
  styleUrls: ['./families-list.component.css']
})
export class FamiliesListComponent implements OnInit {
families :Family[];
family : Family;
errorMessage = '';
search : string;
accessFamilyEdit: boolean = true;
private roles: string[];
p: number = 1;

  constructor(private router:Router,private familyservi: FamilyServiceService,private toastr: ToastrService,private tokenStorage: TokenStorageService) { }
  
  
  ngOnInit() {
    this.searchFamily();
     if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      if (this.roles[1] === '1' ) {
          this.accessFamilyEdit = false;
        }
     }
  }

  searchFamily(){
    this.familyservi.getFamily()
    .subscribe(data => {
    this.families = data;
    this.family=this.families[0]
   });
}

    edit(family:Family):void{
    localStorage.setItem("id",family.id.toString());
    this.router.navigate(["family/view"]);
  }
    
    delete(fam:Family,family:Family): void{
      this.familyservi.deleteFamily(fam) 
      .subscribe(data => {
        this.families = this.families.filter(p=>p!==family); 
         this.toastr.success ("Familia eliminada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );    
    }
    
     addFamily(family:Family){
      this.family=family;     
  }
  
   clear() {
    this.search = '';
  }
}