import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpRequest,HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Scale } from '../../Models/scale.model';
import { Family } from '../../Models/family.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class UpdateScaleService {

  private httpOptions: any;


  constructor(private http: HttpClient,private env: EnvService) {}
  
  Url=this.env.apiUrl+'/updateScale';
  
     public getOnline(scales) {
       return this.http.put<Scale[]>(this.Url,scales);
    } 
    
    public saveLayout(scales: Scale[],families : Family []){
        console.log(scales)
        console.log(families)
        return this.http.post(this.Url,{families,scales});
    }
   
   }

