import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EnvService } from '../env.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService{

  private httpOptions: any;


  constructor(private http: HttpClient,private env: EnvService) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json',
                                'Access-Control-Allow-Origin':'*',
                                'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept, Authorization',
                                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH'
                              })
  };
   }

 Url=this.env.apiUrl+'/login';
 
  login(username:string, password:string) {
    return this.http.post(this.Url, {
      users: username,
      password: password,     
    });     
  }
}