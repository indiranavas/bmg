import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Scale } from '../../Models/scale.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class ScaleServiceService {

  private httpOptions: any;

  constructor(private http: HttpClient,private env: EnvService) {}
  
  Url=this.env.apiUrl+'/scale';
  
  public deleteScale(scale: Scale){     
       return this.http.delete(this.Url+"/"+scale.id); 
  }
  
  public getScaleId(id) {   
    return this.http.get<number>(this.Url+"/"+id);
  } 
  
}
