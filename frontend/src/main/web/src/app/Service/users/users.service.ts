import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Users } from '../../Models/users.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private httpOptions: any;


  constructor(private http: HttpClient) {}

Url='http://localhost:8080/backend-0.0.1-SNAPSHOT/user';


   public getUser(){
       return this.http.get<Users[]>(this.Url); 
   }

  getUserId(id){
    return this.http.get<Users>(this.Url+"/"+id)
  }
  
  public saveUser(user : Users) {
    return this.http.post<Users>(this.Url, user);
  }  

  public updateUser(user : Users) {    
    return this.http.put(this.Url+"/"+user.id, user);
  }
  
   public deleteUser(id) {      
      return this.http.delete<Users>(this.Url+"/"+id);    
   }
}


