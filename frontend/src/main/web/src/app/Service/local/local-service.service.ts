import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Local } from '../../Models/local.model';
import { Scale } from '../../Models/scale.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class LocalServiceService {

  private httpOptions: any;

  constructor(private http: HttpClient,private env: EnvService) {}
  
  Url=this.env.apiUrl+'/store';
  
       
 public getLocal() {
       return this.http.get<Local[]>(this.Url);   
   }

 public getLocalId(id) {   
    return this.http.get<{local : Local, scales: Scale[]}>(this.Url+"/"+id);
  } 
   
 public saveLocal(local : Local, scales: Scale[]) {
    return this.http.post<Local>(this.Url,{local, scales});
  }

  public updateLocal(local : Local, scales: Scale[]) {
    return this.http.put<Local>(this.Url+"/"+local.id, {local, scales});
  }
  
   public deleteLocal(local) {
      return this.http.delete<Local>(this.Url+"/"+local.id);     
  }
}
