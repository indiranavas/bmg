import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpRequest,HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../../Models/product.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient,private env: EnvService) { }
  
   Url=this.env.apiUrl+'/product';
  

   public getProduct() {   
    return this.http.get<Product[]>(this.Url); 
   }

 public getProductId(id) {   
    return this.http.get<Product>(this.Url+"/"+id);
  }  

 public saveProduct(product : Product) {
    return this.http.post<Product>(this.Url, product);
  }  

  public updateProduct(product : Product) {
    return this.http.put(this.Url+"/"+product.id, product);
  }
  
   public deleteProduct(product : Product) {    
      return this.http.delete<Product>(this.Url+"/"+product.id);           
  }

  public pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', this.Url+'/upload', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }
  
 

  }