import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpRequest,HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Family } from '../../Models/family.model';
import { Product } from '../../Models/product.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class FamilyServiceService {

  private httpOptions: any;


  constructor(private http: HttpClient,private env: EnvService) {}
  
  
  Url=this.env.apiUrl+'/family';
  
   public getFamily() {
       return this.http.get<Family[]>(this.Url);   
   }

 public getFamilyId(id) {   
    return this.http.get<{family : Family, products: Product[]}>(this.Url+"/"+id);
  } 
   
 public saveFamily(family : Family, products: Product[]) {
    return this.http.post<Family>(this.Url,{family, products});
  }

  public updateFamily(family : Family, products: Product[]) {
    return this.http.put(this.Url+"/"+family.id, {family, products});
  }
  
   public deleteFamily(family : Family) {   
      return this.http.delete<Family>(this.Url+"/"+family.id);              
  }
  
  public pushFileToStorage(file: File,url): Observable<HttpEvent<{}>> {
  const formdata: FormData = new FormData();
  formdata.append('file', file); 
  formdata.append('url', url);  
  console.log(formdata)
  const req = new HttpRequest('POST', this.Url+'/upload', formdata, {
    reportProgress: true,
    responseType: 'text'
  });
  return this.http.request(req);
}

  }

