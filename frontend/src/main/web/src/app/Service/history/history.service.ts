import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { History } from '../../Models/history.model';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {

  private httpOptions: any;


  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json',
                                'Access-Control-Allow-Origin':'*',
                                'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept, Authorization',
                                'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH'
                              })
  };
   }

   public getHistory(): Observable<History> {
    const url = `http://localhost:9898/layout`;

    return this.http.get<History>(url); 
   }

  /*
  updateLayout(history : History): Observable<History> {    
    console.log(history)
    const url = `http://localhost:9898/history`;
    return this.http.put<History>(url,history);
    console.log("Estoy en el servicio -fin")
  }
  */

  getDetalle(id){
    console.log("en el servicio")
    return this.http.get('http://localhost:8080/history/'+id).subscribe()
  }  

  public setHistory(layout : History) {
    return this.http
      .post(`http://localhost:9898/history`, history, this.httpOptions).subscribe()
  }

  public postHistory(layout : History) {
    return this.http
    .post(`http://localhost:9898/history`, history, this.httpOptions).subscribe()

  }

  public puthistory(layout : History) {
    return this.http
    .put(`http://localhost:9898/history`, history, this.httpOptions).subscribe()

  }
/*
  public deleteHistory(history : History):Observable<Layout> {
    return this.http
    .delete(`http://localhost:9898/history`, history this.httpOptions).subscribe()

  }
*/

 
  }
