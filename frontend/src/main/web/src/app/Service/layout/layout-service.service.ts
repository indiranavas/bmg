import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Layout } from '../../Models/layouts.model';
import { Family } from '../../Models/family.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class LayoutServiceService {

  private httpOptions: any;


  constructor(private http: HttpClient,private env: EnvService) {}
  
  Url=this.env.apiUrl+'/layout';
 
 
   public getLayout() {
    return this.http.get<Layout[]>(this.Url); 
   }

  getLayoutId(id){   
    return this.http.get<{layout : Layout, families: Family[]}>(this.Url+"/"+id)
  }  

 public saveLayout(layout : Layout, families: Family[]) {
    return this.http.post<Layout>(this.Url,{layout, families});
  }

  public updateLayout(layout : Layout, families: Family[]) {
    return this.http.put(this.Url+"/"+layout.id, {layout, families});
  }
  
   public deleteLayout(layout : Layout) {
      return this.http.delete<Layout>(this.Url+"/"+layout.id);  
 }

  }

