
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rol } from '../../Models/rol.model';
import { EnvService } from '../env.service';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  private httpOptions: any;


  constructor(private http: HttpClient,private env: EnvService) {}
  
  Url=this.env.apiUrl+'/rol';
  
   public getRol(){
    return this.http.get<Rol[]>(this.Url); 
   }

  getRolId(id) {  
    return this.http.get<Rol>(this.Url+"/"+id)
  }  

public saveRol(rol : Rol) {
    return this.http.post<Rol>(this.Url, rol);
  }  

  public updateRol(rol : Rol) {
      console.log(rol);
    return this.http.put(this.Url+"/"+rol.id, rol);
  }
  
   public deleteRol(rol : Rol) {       
      return this.http.delete<Rol>(this.Url+"/"+rol.id);  
   }
  }


