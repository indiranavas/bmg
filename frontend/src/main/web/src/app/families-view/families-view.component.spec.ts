import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamiliesViewComponent } from './families-view.component';

describe('FamiliesViewComponent', () => {
  let component: FamiliesViewComponent;
  let fixture: ComponentFixture<FamiliesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamiliesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamiliesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
