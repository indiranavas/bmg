import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import {ToastrService} from  'ngx-toastr' ; 
import { EnvService } from '../Service/env.service';


import { FamilyServiceService } from '../Service/family/family-service.service';
import { Family } from '../Models/family.model';
import { ProductService } from '../Service/product/product.service';
import { Product } from '../Models/product.model';
import { ActivatedRoute , Router } from '@angular/router';
import { TokenStorageService } from '../auth/token-storage.service';

@Component({
  selector: 'app-families-view',
  templateUrl: './families-view.component.html',
  styleUrls: ['./families-view.component.css']
})
export class FamiliesViewComponent implements OnInit {
 family :Family = new Family();
 selectFile : File = null;
 selectProduct: Product[]  = [];
  selectProTemp: Product[];
  products :Product[];
  product: Product = new Product
  productTemp: Product = new Product
   submitted:boolean=false;
   errorMessage = '';
   url:string;
  fileValidae: boolean = false;
  accessFamilyEdit: boolean = true;
private roles: string[];

     familyForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,
      Validators.pattern(/^[0-9]\d{0,5}$/)
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
  
 
 constructor(private router:Router,private familyservi: FamilyServiceService,private productservi: ProductService,private toastr: ToastrService,private env: EnvService,private tokenStorage: TokenStorageService) { }

  ngOnInit() {
      console.log(this.env.apiUrl)
      this.edit();
      this.searchProduct();
      this.url=this.env.apiUrl;
      if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
      if (this.roles[1] === '1' ) {
          this.accessFamilyEdit = false;
        }
     }
      
  }
  
get f() { return this.familyForm.controls; }
 edit(){
      let id= localStorage.getItem("id");
      this.familyservi.getFamilyId(id)
      .subscribe(data=>{
          this.family=data.family;
          this.selectProduct=data.products;
          this.selectProTemp = [];
          for (let i = 0; i < this.selectProduct.length; i++) {
            this.selectProTemp.push(this.selectProduct[i])
          }          
           console.log( this.selectProTemp);
      })
  }
  
     searchProduct(){
      this.productservi.getProduct()
      .subscribe(data => {
        this.products = data;
      });
  }
  
  onFileSelected(event){
    this.selectFile =  event.target.files[0];
    console.log(this.selectFile.type)
    if(this.selectFile.type == "image/jpeg"  || this.selectFile.type == "image/png" || this.selectFile.type== 'image/jpg' || this.selectFile.type== 'image/bmp' ){
        this.fileValidae = true  
  
    } else{
        alert("Formato Invalido")
    }

  }
  
update(family:Family){
       this.submitted = true;
    if (this.familyForm.invalid) {
      return;
    }else{
    if (this.selectFile!=null) {
    this.family.image = this.selectFile.name;
     var formData = new FormData();
  formData.append('file',this.selectFile,this.selectFile.name);   
  console.log(this.selectFile)
  this.familyservi.pushFileToStorage(this.selectFile,this.url)
  .subscribe(data=>{
  
  })
}
    this.familyservi.updateFamily(family,this.selectProduct)
    .subscribe(data=>{
        this.family=data as any;
        this.router.navigate(["/family"]);
         this.toastr.success ("Familia modificada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });               
      }
      );
}
}

   addProducts(){
       console.log(this.selectProduct);
      this.selectProduct = [];
       console.log(this.selectProTemp);
//      console.log(this.selectProduct);
       for (let i = 0; i < this.selectProTemp.length; i++) {
        this.selectProTemp[i].checked=false  
       this.selectProduct.push(this.selectProTemp[i])
   }
//   this.selectProTemp=[]
//      console.log(this.selectProduct);
  }
  
   addProductoTemp(add, check){ 
        console.log(this.selectProTemp);   
    if(add != undefined){
        add.checked=true  
      if(check.target.checked)
        this.selectProTemp.push(add);
      if(!check.target.checked){
          add.checked=false
        this.selectProTemp=this.selectProTemp.filter(function(value){
          return value != add;
        });
      }
        console.log(this.selectProTemp);
    }
  }
  
    removeProduct(pro){
      var indice = this.selectProduct.indexOf(pro); 
      this.selectProduct.splice(indice,1);
  }
  
     changeProduct(){
      var indice = this.selectProduct.indexOf(this.product); 
      this.selectProduct.splice(indice,1);
      console.log(this.selectProduct)
      this.productTemp.checked=false;
      this.selectProduct.splice(indice,0,this.productTemp)
      this.selectProTemp=[]
      for (let i = 0; i < this.selectProduct.length; i++) {
         this.selectProTemp.push(this.selectProduct[i])
      }
      console.log(this.selectProduct)  
      console.log(this.selectProTemp)   
     
}
     addProduct(product:Product){
      this.product=product;     
  }
  
  changeProductTemp(productTemp,check){
     if(check.target.checked)
      productTemp.checked=true
     if(!check.target.checked)
           productTemp.checked=false 
      this.productTemp=productTemp 
      console.log(this.productTemp)     
  }
  
  
   clearProducts(){
       this.selectProTemp = [];              
       for (let i = 0; i <  this.products.length; i++) {
       this.products[i].checked=false;
 }
} 

}
