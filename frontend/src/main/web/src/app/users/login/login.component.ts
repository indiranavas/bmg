import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../auth/auth.service';
import { TokenStorageService } from '../../auth/token-storage.service';
import { Login } from '../../Models/login.model';
import { Route, Router } from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  private loginInfo: Login;

  constructor(private authService: AuthService, private tokenStorage: TokenStorageService,private router: Router,private toastr: ToastrService) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
    }
  }

  onSubmit() {
    console.log(this.form);

    this.loginInfo = new Login(
      this.form.username,
      this.form.password);

    this.authService.attemptAuth(this.loginInfo).subscribe(
      data => {
          console.log(data)
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUsername(data.userDetails.userName);
        this.tokenStorage.saveAuthorities(data.authorities);
        this.tokenStorage.saveName(data.userDetails.name);
        this.tokenStorage.saveLastname(data.userDetails.lastName);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getAuthorities();
        this.reloadPage();
        console.log(this.roles);
      },
      error => {
        console.log(error);
         this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        }); 
      }
    );
  }

  reloadPage() {    
    if (this.roles[4] != '0' ) {
        this.router.navigateByUrl('/panel');
    }else if (this.roles[5] != '0') {
        this.router.navigateByUrl('/tienda');
    }else if (this.roles[2] != '0') {
        this.router.navigateByUrl('/layout');
    }else if (this.roles[6] != '0') {
        this.router.navigateByUrl('/layouts');
    }else if (this.roles[3] != '0') {
        this.router.navigateByUrl('/product');
    }else if (this.roles[1] != '0') {
        this.router.navigateByUrl('/family');
    }else if (this.roles[7] != '0') {
        this.router.navigateByUrl('/user');
    }else if (this.roles[0] != '0') {
        this.router.navigateByUrl('/actividades');
    }
    
  }
}
