import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;

import { RolService } from '../../../Service/rol/rol-service';
import { Rol } from '../../../Models/rol.model';
import { TokenStorageService } from '../../../auth/token-storage.service';

@Component({
  selector: 'app-rol-lis',
  templateUrl: './rol-lis.component.html',
  styleUrls: ['./rol-lis.component.css']
})
export class RolLisComponent implements OnInit {
rols :Rol[];
rol:Rol= new Rol
errorMessage = '';
 accessAdministrationEdit: boolean = true;
private roles: string[];

  constructor(private rolservi: RolService,private router:Router,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }
  
 
  ngOnInit() {
    this.searchRol();
    if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
         if (this.roles[7] === '1' ) {
          this.accessAdministrationEdit = false;
        }
     }
  }
  
  searchRol(){
    this.rolservi.getRol()
      .subscribe(data => {
        this.rols = data;
      });
    }

edit(rol:Rol):void{
    localStorage.setItem("id",rol.id.toString());
    this.router.navigate(["rol/view"]);
    
  }
    
    delete(rol:Rol): void{
      this.rolservi.deleteRol(rol) 
      .subscribe(data => {
        this.rols = this.rols.filter(p=>p!==rol); 
             this.toastr.success ("Rol eliminado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });               
      }
      ); 
    }
    
     addRol(rol:Rol){
      this.rol=rol;     
  }

}