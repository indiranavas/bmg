import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolLisComponent } from './rol-lis.component';

describe('RolLisComponent', () => {
  let component: RolLisComponent;
  let fixture: ComponentFixture<RolLisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolLisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolLisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
