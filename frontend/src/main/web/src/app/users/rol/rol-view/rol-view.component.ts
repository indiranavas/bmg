import { Component, OnInit } from '@angular/core';
import { Rol } from '../../../Models/rol.model';
import { RolService } from '../../../Service/rol/rol-service';
import { ActivatedRoute , Router } from '@angular/router';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms'
import { TokenStorageService } from '../../../auth/token-storage.service';



@Component({
  selector: 'app-rol-view',
  templateUrl: './rol-view.component.html',
  styleUrls: ['./rol-view.component.css']
})
export class RolViewComponent implements OnInit {

  constructor(private router:Router,private roleService: RolService,
   private rutaActiva: ActivatedRoute,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }

role: Rol = new Rol();
accessStatistics:boolean=false;
accessStatisticsView:boolean=false;
accessStore:boolean=false;
accessStoreView:boolean=false;
accessStoreEdit:boolean=false;
accessLayout:boolean=false;
accessLayoutView:boolean=false;
accessLayoutEdit:boolean=false;
accessUpdateScale:boolean=false;
accessUpdateScaleEdit:boolean=false;
accessProduct:boolean=false;
accessProductView:boolean=false;
accessProductEdit:boolean=false;
accessFamily:boolean=false;
accessFamilyView:boolean=false;
accessFamilyEdit:boolean=false;
accessAdministration:boolean=false;
accessAdministrationView:boolean=false;
accessAdministrationEdit:boolean=false;
accessActivity:boolean=false;
accessActivityView:boolean=false;
error : boolean = false;
submitted:boolean=false;
  errorMessage = '';
  accessAdministrationEditLog: boolean = true;
private roles: string[];

      roleForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,     
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
  
  ngOnInit() {
      this.edit();
      if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
         if (this.roles[7] === '1' ) {
          this.accessAdministrationEditLog = false;
        }
     }
  }
  
  get f() { return this.roleForm.controls; }
  
edit(){
   let id= localStorage.getItem("id");
   this.roleService.getRolId(id)
       .subscribe(data => {
        this.role = data;
        if (this.role.accessStatistics==0) {
          this.accessStatistics=true;
        }else if (this.role.accessStatistics==1) {
          this.accessStatisticsView=true;
        }
         if (this.role.accessStore==0) {
          this.accessStore=true;
        }else if (this.role.accessStore==1) {
          this.accessStoreView=true;
        }else if (this.role.accessStore==2) {
          this.accessStoreEdit=true;
        }     
        if (this.role.accessLayout==0) {
          this.accessLayout=true;
        }else if (this.role.accessLayout==1) {
          this.accessLayoutView=true;
        }else if (this.role.accessLayout==2){
             this.accessLayoutEdit=true;
        }
         if (this.role.accessUpdateScale==0) {
          this.accessUpdateScale=true;
        }else if (this.role.accessUpdateScale==2) {
          this.accessUpdateScaleEdit=true;
        }
        if (this.role.accessProduct==0) {
          this.accessProduct=true;
        }else if (this.role.accessProduct==1) {
          this.accessProductView=true;
        }else if (this.role.accessProduct==2){
             this.accessProductEdit=true;
        }
        if (this.role.accessFamily==0) {
          this.accessFamily=true;
        }else if (this.role.accessFamily==1) {
          this.accessFamilyView=true;
        }else if (this.role.accessFamily==2){
             this.accessFamilyEdit=true;
        }
        if (this.role.accessAdministration==0) {
          this.accessAdministration=true;
        }else if (this.role.accessAdministration==1) {
          this.accessAdministrationView=true;
        }else if (this.role.accessAdministration==2){
             this.accessAdministrationEdit=true;
        }
        if (this.role.accessActivity==0) {
          this.accessActivity=true;
        }else if (this.role.accessActivity==1) {
          this.accessActivityView=true;
        }
        console.log(this.role);
           
      })
    }
   
    update(rol:Rol){
         this.submitted = true;
    if (this.roleForm.invalid) {
      return;
    }else{
        console.log(rol)
         this.roleService.updateRol(rol)
    .subscribe(data=>{
        this.role=data as any;
        this.router.navigate(["/rol"]);
       this.toastr.success ("Rol modificado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
    }


addAcceso(rol:Rol,value,check){   
    console.log(value);
    console.log(check.target.name);
    let acceso = check.target.name;
    if (acceso=="accessStatistics") {
        this.role.accessStatistics=value;
}else if (acceso=="accessStore") {
      this.role.accessStore=value;
}else if (acceso=="accessLayout") {
      this.role.accessLayout=value;
}else if (acceso=="accessUpdateScale") {
      this.role.accessUpdateScale=value;
}else if (acceso=="accessProduct") {
      this.role.accessProduct=value;
}else if (acceso=="accessFamily") {
      this.role.accessFamily=value;
}else if (acceso=="accessAdministration") {
      this.role.accessAdministration=value;
}else if (acceso=="accessActivity") {
      this.role.accessActivity=value;
}
 console.log(this.role);  
}
}
