import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';

import { RolService } from '../../../Service/rol/rol-service';
import { Rol} from '../../../Models/rol.model';
import { Route, Router } from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms'

@Component({
  selector: 'app-rol-new',
  templateUrl: './rol-new.component.html',
  styleUrls: ['./rol-new.component.css']
})
export class RolNewComponent implements OnInit {

  rol: Rol = new Rol()
  accessStatistics:boolean=true;
accessStatisticsView:boolean=false;
accessStore:boolean=true;
accessStoreView:boolean=false;
accessStoreEdit:boolean=false;
accessLayout:boolean=true;
accessLayoutView:boolean=false;
accessLayoutEdit:boolean=false;
accessUpdateScale:boolean=true;
accessUpdateScaleEdit:boolean=false;
accessProduct:boolean=true;
accessProductView:boolean=false;
accessProductEdit:boolean=false;
accessFamily:boolean=true;
accessFamilyView:boolean=false;
accessFamilyEdit:boolean=false;
accessAdministration:boolean=true;
accessAdministrationView:boolean=false;
accessAdministrationEdit:boolean=false;
accessActivity:boolean=true;
accessActivityView:boolean=false;
  submitted:boolean=false;
  errorMessage = '';
  roleForm = new FormGroup({
      code: new FormControl('',[
      Validators.required,     
    ]),description: new FormControl('',[
      Validators.required
    ])
  });

  constructor(private router:Router, private rolservi: RolService,private toastr: ToastrService) { }

  ngOnInit() {

  }
  
  get f() { return this.roleForm.controls; }
  
save(){
     this.submitted = true;
    if (this.roleForm.invalid) {
      return;
    }else{
     this.rolservi.saveRol(this.rol)
      .subscribe(data=>{           
          this.router.navigate(["/rol"])
      this.toastr.success ("Rol creado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
}
}

addAcceso(rol:Rol,value,check){   
    console.log(value);
    console.log(check.target.name);
    let acceso = check.target.name;
    if (acceso=="accessStatistics") {
        this.rol.accessStatistics=value;
}else if (acceso=="accessStore") {
      this.rol.accessStore=value;
}else if (acceso=="accessLayout") {
      this.rol.accessLayout=value;
}else if (acceso=="accessUpdateScale") {
      this.rol.accessUpdateScale=value;
}else if (acceso=="accessProduct") {
      this.rol.accessProduct=value;
}else if (acceso=="accessFamily") {
      this.rol.accessFamily=value;
}else if (acceso=="accessAdministration") {
      this.rol.accessAdministration=value;
}else if (acceso=="accessActivity") {
      this.rol.accessActivity=value;
}
 console.log(this.rol);  
}

}







  
  