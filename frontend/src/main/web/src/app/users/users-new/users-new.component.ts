import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { MustMatch } from '../must-match.validator';

import { UsersService } from '../../Service/users/users.service';
import { Users } from '../../Models/users.model';
import { Route, Router } from '@angular/router';
import { RolService} from '../../Service/rol/rol-service';
import { Rol} from '../../Models/rol.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-users-new',
  templateUrl: './users-new.component.html',
  styleUrls: ['./users-new.component.css']
})
export class UsersNewComponent implements OnInit {

  rols :Rol[];
  userForm: FormGroup;

   user: Users = new Users();
   submitted:boolean=false;
  errorMessage = '';



  constructor(private router:Router, private usersservi: UsersService, private roleService: RolService,private toastr: ToastrService,private formBuilder: FormBuilder) { }


  ngOnInit() {
        this.userForm = this.formBuilder.group({
      userName:['',Validators.required],
      name:['',Validators.required],
      lastName:['',Validators.required],
      password:['',Validators.required],
      rol:['', Validators.required],
      confirmpassword:['',Validators.required]
  },{
      validator:MustMatch('password', 'confirmpassword')  
 });
         this.searchRol();
       }
       
get f() { return this.userForm.controls; }
       
searchRol(){
    this.roleService.getRol()    
      .subscribe(data => {
        this.rols = data;
        console.log(this.rols)
      });
    }
    
 save(){
       this.submitted = true;
    if (this.userForm.invalid) {
      return;
    }else{
    console.log(this.user)
     this.usersservi.saveUser(this.user)
      .subscribe(data=>{           
          this.router.navigate(["/user"])
       this.toastr.success ("Usuario creado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
}
 }
 
    changeRol(value){  
        this.user.rolId= value;      
      }

  }





 
  

  
  

  