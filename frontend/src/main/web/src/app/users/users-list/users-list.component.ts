import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;

import { UsersService } from '../../Service/users/users.service';
import {  Users } from '../../Models/users.model';
import {Router} from '@angular/router';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  constructor(private usersservi: UsersService,private router:Router,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }
  users :Users[]; 
  errorMessage = '';
  user:Users= new Users
  accessAdministrationEdit: boolean = true;
private roles: string[];

  ngOnInit() {
    this.searchUsers();
     if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
         if (this.roles[7] === '1' ) {
          this.accessAdministrationEdit = false;
        }
     }
}

searchUsers(){
  this.usersservi.getUser()
      .subscribe(data => {
          console.log(data)
        this.users = data;
        this.user=this.users[0]
        console.log(this.users)
      });
  }

edit(user:Users):void{
    localStorage.setItem("id",user.id.toString());
    this.router.navigate(["user/view"]);    
  }
    
    delete(id,user:Users): void{
      this.usersservi.deleteUser(id) 
      .subscribe(data => {
        this.users = this.users.filter(p=>p!==user); 
       this.toastr.success ("Usuario eliminado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });               
      }
      ); 
    }

 addUser(user:Users){
      this.user=user;
      console.log(this.user)     
  }
  
}

