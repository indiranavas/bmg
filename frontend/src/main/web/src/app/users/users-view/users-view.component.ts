import { Component, OnInit } from '@angular/core';

import { UsersService } from '../../Service/users/users.service';
import { Users } from '../../Models/users.model';
import { Route, Router } from '@angular/router';
import { RolService} from '../../Service/rol/rol-service';
import { Rol} from '../../Models/rol.model';
import { Observable } from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule,FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { MustMatch } from '../must-match.validator';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-users-view',
  templateUrl: './users-view.component.html',
  styleUrls: ['./users-view.component.css']
})
export class UsersViewComponent implements OnInit {

 user: Users = new Users();
active:boolean=false;
blocked:boolean=false;
  rols :Rol[];
   submitted:boolean=false;
  errorMessage = '';
  userForm: FormGroup;
   testForm: FormGroup;
  accessAdministrationEdit: boolean = true;
private roles: string[];
 
  constructor(private router:Router, private usersservi: UsersService, private roleService: RolService,private toastr: ToastrService,private formBuilder: FormBuilder, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
      this.edit();
      this.searchRol();
      this.userForm = this.formBuilder.group({
      userName:['',Validators.required],
      name:['',Validators.required],
      lastName:['',Validators.required],      
      rol:['', Validators.required],
       newPassword:['', Validators.required],
       status:[''],
      confirmpassword:['', Validators.required]
  }
   
//  ,{
//      validator:MustMatch('password', 'confirmpassword')  
// }
 );
 console.log(this.user.password);
    console.log(this.userForm);
 this.setNewPassword();
      this.edit();
      this.searchRol();
       if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
        if (this.roles[7] === '1' ) {
          this.accessAdministrationEdit = false;
        }
     }
  }
  
  setNewPassword(){    
     const confirmpassword = this.userForm.get('confirmpassword');
      const newPassword = this.userForm.get('newPassword');
    newPassword.valueChanges.subscribe(result => {
        if (result) {
//          newPassword.setValidators(Validators.required);
//          newPassword.updateValueAndValidity();
          confirmpassword.setValidators(Validators.required);
          validator:MustMatch(this.userForm.get('newPassword').value, this.userForm.get('confirmpassword').value)  
         
          confirmpassword.updateValueAndValidity();
}else{
//    newPassword.setValidators(null);
//          newPassword.updateValueAndValidity();
           confirmpassword.setValidators(null);
          confirmpassword.updateValueAndValidity();
}
          
       });
     }
  
  
  get f() { return this.userForm.controls; }
  
  edit(){
      let id= localStorage.getItem("id");
      console.log(id);
      this.usersservi.getUserId(id)
      .subscribe(data=>{
          this.user=data;
          if (this.user.status==true) {
             this.active=true;
          }else if (this.user.status==false) {
            this.blocked=true;
            }
         })
  }
  
  update(user:Users){  
      console.log(user)   
           this.submitted = true;
    if (this.userForm.invalid) {
         console.log(this.userForm)
      return;
    }else{
     console.log("else")
       this.usersservi.updateUser(user)
    .subscribe(data=>{
        this.user=data as any;
        this.router.navigate(["/user"]);
          this.toastr.success ("Usuario modificado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
  }
  
  searchRol(){
    this.roleService.getRol()    
      .subscribe(data => {
        this.rols = data;
        console.log(this.rols)
      });
    }

editStatus(value){
    this.user.status=value
    console.log(value);
}

changeStatus(value){
    console.log(value)
if (value==0) {
     this.user.status= false
     this.active=false;
     this.blocked=true;
}else{
    this.user.status= true
     this.active=true;
     this.blocked=false;
     
}
   
}

}
