import { Component, OnInit } from '@angular/core';

import { LayoutServiceService } from '../../Service/layout/layout-service.service';
import { FamilyServiceService } from '../../Service/family/family-service.service';
import { Route, Router } from '@angular/router';
import { Layout} from '../../Models/layouts.model';
import { Family} from '../../Models/family.model';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-layouts-view',
  templateUrl: './layouts-view.component.html',
  styleUrls: ['./layouts-view.component.css']
})
export class LayoutsViewComponent implements OnInit {

  constructor(private router:Router, private layoutservi: LayoutServiceService,private famiservi: FamilyServiceService,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }
  
page : any = [{'name':'1'}];
layout: Layout = new Layout()
  family : Family= new Family()
   selectFamily :any = []
  idBorrar : string;
   families :Family[];
  selectFamilyTemp: Family[];
    submitted:boolean=false;
   errorMessage = '';
    familyModal : Family= new Family()
    accessLayoutEdit: boolean = true;
private roles: string[];
   
     layoutForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,     
    ]),name: new FormControl('',[
      Validators.required
    ])
  });
   
   
  ngOnInit() {
      this.edit();
       this.selectFamily = [];
    this.selectFamilyTemp = [];
      this.searchFamilies();
        if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
       if (this.roles[2] === '1' ) {
          this.accessLayoutEdit = false;        
        }
     }
  }
  
  get f() { return this.layoutForm.controls; }
  
  searchFamilies(){
      this.famiservi.getFamily()
      .subscribe(data => {
        this.families = data;
      });
  }
  
edit(){
      let id= localStorage.getItem("id");     
      this.layoutservi.getLayoutId(id)
      .subscribe(data=>{
          this.layout=data.layout;
          this.selectFamily=data.families;
      })
  }
  
  update(layout:Layout){
      this.submitted = true;
    if (this.layoutForm.invalid) {
      return;
    }else{
      this.layoutservi.updateLayout(layout,this.selectFamily)
    .subscribe(data=>{
//        this.deleteScale();
        this.layout=data as any;
        this.router.navigate(["/layout"]);
         this.toastr.success ("Layout modificado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
}
  }
  
  addFamily(){
//    if(this.selectFamilyTemp.length>0){
      this.selectFamily.push(this.family);
       this.family.checked=false 
      console.log(this.selectFamily);
//    }   
  }
  
  addFamilyTemp(add, check){ 
      if(check.target.checked)
      add.checked=true
     if(!check.target.checked)
           add.checked=false    
    this.family=add;     
    console.log(this.family);
  }
  
      clearFamily(){
       this.family = new Family;              
       for (let i = 0; i <  this.families.length; i++) {
       this.families[i].checked=false;
 }
}

  addfamilia(family:Family){
      this.familyModal=family;     
  }
  
    removeFamily(family){
      var indice = this.selectFamily.indexOf(family); 
      this.selectFamily.splice(indice,1);
//          this.selectProTemp=this.selectProTemp.filter(function(value){
//          return value != family;
//        });
//      this.scaleDelete.push(sca);

  }

agregarPage(){
  let x = { 
"name": this.page.length + 1
}
   this.page.push(x);
   console.log(this.page);
}
removePage(){
  this.page.pop()
  console.log(this.page);
  }
  asignarId(id){
  this.idBorrar=id;     
}
  newPage(add, page){
  // event.target.checked
  if(add != undefined){  
    if(page.target.page)
      this.selectFamilyTemp.push(add);
    if(!page.target.checked){
      this.selectFamilyTemp=this.selectFamilyTemp.filter(function(value){
        return value != add;
      });
    }
      console.log(this.selectFamilyTemp);
  }

  }
}