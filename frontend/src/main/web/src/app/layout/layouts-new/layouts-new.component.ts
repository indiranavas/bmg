import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';

import { LayoutServiceService } from '../../Service/layout/layout-service.service';
import { FamilyServiceService } from '../../Service/family/family-service.service';
import { Route, Router } from '@angular/router';
import { Layout} from '../../Models/layouts.model';
import { Family} from '../../Models/family.model';


@Component({
  selector: 'app-layouts-new',
  templateUrl: './layouts-new.component.html',
  styleUrls: ['./layouts-new.component.css']
})
export class LayoutsNewComponent implements OnInit {

  page : any = [{'name':'1'}];
  layout: Layout = new Layout()
  selectFamily: Family[];
  selectFamilyTemp: Family[];
  families :Family[];
  family:Family= new Family;
  idBorrar : string;
  familydelete:Family= new Family;
  submitted:boolean=false;
  errorMessage = '';
   layoutForm = new FormGroup({
    code: new FormControl('',[
      Validators.required      
    ]),name: new FormControl('',[
      Validators.required
    ])
  });
  

  constructor(private router:Router, private layoutservi: LayoutServiceService,private famiservi: FamilyServiceService,private toastr: ToastrService) { }

  ngOnInit() {
      this.selectFamily = [];
    this.selectFamilyTemp = [];
      this.searchFamilies();
  }
  
 get f() { return this.layoutForm.controls; }
  
searchFamilies(){
      this.famiservi.getFamily()
      .subscribe(data => {
        this.families = data;
        console.log(this.families)
      });
  }
  
  save(){
       this.submitted = true;
    if (this.layoutForm.invalid) {
      return;
    }else{    
    console.log(this.selectFamily) 
      this.layoutservi.saveLayout(this.layout,this.selectFamily)
      .subscribe(data=>{           
          this.router.navigate(["/layout"])
          this.toastr.success ("Layout creado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
  }
    clearFamily(){
       this.family = new Family;              
       for (let i = 0; i <  this.families.length; i++) {
       this.families[i].checked=false;
 }
}

  addFamily(){
      this.selectFamily.push(this.family);
       this.family.checked=false
      console.log(this.family);  
  }
  
  addFamilyTemp(add, check){
      console.log(add)
      if(check.target.checked)
      add.checked=true
     if(!check.target.checked)
           add.checked=false         
    this.family=new Family(add);  
    console.log(this.family);
 }
 
  addFamilyDelete(family:Family){
      this.familydelete=family;     
  }
 
 
   removeFamily(fam){
      var indice = this.selectFamily.indexOf(fam); 
      this.selectFamily.splice(indice,1);
          this.selectFamilyTemp=this.selectFamilyTemp.filter(function(value){
          return value != fam;
        });
//      this.scaleDelete.push(sca);

  }
  agregarPage(){
  let x = { 
"name": this.page.length + 1
}
   this.page.push(x);
   console.log(this.page);
}

removePage(){
  this.page.pop()
  console.log(this.page);
  }
  asignarId(id){
  this.idBorrar=id;     
}
  newPage(add, page){
  // event.target.checked
  if(add != undefined){  
    if(page.target.page)
      this.selectFamilyTemp.push(add);
    if(!page.target.checked){
      this.selectFamilyTemp=this.selectFamilyTemp.filter(function(value){
        return value != add;
      });
    }
      console.log(this.selectFamilyTemp);
  }
}
} 


  

 
