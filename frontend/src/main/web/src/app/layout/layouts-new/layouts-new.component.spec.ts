import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutsNewComponent } from './layouts-new.component';

describe('LayoutsNewComponent', () => {
  let component: LayoutsNewComponent;
  let fixture: ComponentFixture<LayoutsNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayoutsNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutsNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
