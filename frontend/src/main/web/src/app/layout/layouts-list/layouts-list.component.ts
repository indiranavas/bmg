import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr';

import { LayoutServiceService } from '../../Service/layout/layout-service.service';
import { Layout } from '../../Models/layouts.model';
import { Route, Router } from '@angular/router';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-layouts-list',
  templateUrl: './layouts-list.component.html',
  styleUrls: ['./layouts-list.component.css']
})
export class LayoutsListComponent implements OnInit {
layouts :Layout[]; 
id:string;
laydelete:Layout;
errorMessage = '';
accessLayoutEdit: boolean = true;
private roles: string[];
search : string;
  constructor(private router:Router,private layoutservi: LayoutServiceService,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }
 
  error : boolean = false;
//  dtOptions: DataTables.Settings = {};
 
   ngOnInit() {
    this.searchLayout();
      if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
        if (this.roles[2] === '1' ) {
          this.accessLayoutEdit = false;        
        }
     }
  }

  searchLayout(){
    this.layoutservi.getLayout()
    .subscribe(data => {
    this.layouts = data;
    this.laydelete=this.layouts[0]
   });
     
  }
  
  edit(layout:Layout):void{
    localStorage.setItem("id",layout.id.toString());
    this.router.navigate(["layout/view"]);
  }
    
    delete(layout:Layout,laydelete:Layout): void{
      this.layoutservi.deleteLayout(layout) 
      .subscribe(data => {
          console.log(data)
          console.log(layout)
          console.log(this.layouts)
           console.log(this.laydelete)
        this.layouts = this.layouts.filter(p=>p!==laydelete); 
        this.toastr.success ("Layout eliminado correctamente!", 'Success' ,{
            timeOut: 2000,
            closeButton: true
        });
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });        
      }
      );
    }
    
    addLayout(add, check){
        if(add != undefined){  
      if(check.target.checked)
        this.id= add.id.toString();
      if(!check.target.checked){
        this.id="";
      }
      console.log(this.id);
    }
  }
  
   addLayoutDelete(layout:Layout){
      this.laydelete= layout;         
  }
    
}