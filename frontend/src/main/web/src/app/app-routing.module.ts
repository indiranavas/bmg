import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpanelComponent } from './mainpanel/mainpanel.component';

// Tienda
import {LocalListComponent} from './local/local-list/local-list.component';
import {LocalNewComponent} from './local/local-new/local-new.component';
import {LocalViewComponent} from './local/local-view/local-view.component';
// end

// layout
import {LayoutsListComponent} from './layout/layouts-list/layouts-list.component';
import {LayoutsNewComponent} from './layout/layouts-new/layouts-new.component';
import {LayoutsViewComponent} from './layout/layouts-view/layouts-view.component';
import {LayoutComponent} from './balanza/layout/layout.component';

// producto
import {ProductsListComponent} from './product/products-list/products-list.component';
import {ProductsNewComponent} from './product/products-new/products-new.component';  
import {ProductsViewComponent} from './product/products-view/products-view.component';

// user
import {UsersListComponent} from './users/users-list/users-list.component';
import {UsersNewComponent} from './users/users-new/users-new.component';
import {UsersViewComponent} from './users/users-view/users-view.component';

// rol
import {RolLisComponent} from './users/rol/rol-lis/rol-lis.component';
import {RolNewComponent} from './users/rol/rol-new/rol-new.component';
import {RolViewComponent} from './users/rol/rol-view/rol-view.component';

// family
import {FamiliesListComponent} from './families-list/families-list.component';
import {FamiliesNewComponent} from './families-new/families-new.component';
import {FamiliesViewComponent} from './families-view/families-view.component';

//history
import {HistoryComponent} from './template/history/history.component';

//login
import {LoginComponent} from './users/login/login.component';


const routes: Routes = [
  {path: 'panel', component: MainpanelComponent},
  // Tienda router
    {path: 'tienda', component: LocalListComponent},
    {path: 'tienda/new', component : LocalNewComponent },
    {path: '',redirectTo: '/panel',pathMatch: 'full'},
    {path: 'tienda/view', component: LocalViewComponent},  
  // end

  // layout
    {path: 'layout', component : LayoutsListComponent},
    {path: 'layout/new', component: LayoutsNewComponent},
    {path: 'layout/view', component: LayoutsViewComponent},
    {path: 'layouts', component: LayoutComponent},

  // product
    {path: 'product', component: ProductsListComponent},
    {path: 'product/new', component: ProductsNewComponent},
    {path: 'product/view', component: ProductsViewComponent},

  // user
    {path: 'user', component: UsersListComponent},
    {path: 'user/new', component: UsersNewComponent},
    {path: 'user/view', component: UsersViewComponent},

  // rol
    {path: 'rol', component: RolLisComponent},
    {path: 'rol/new', component: RolNewComponent},
    {path: 'rol/:id', component: RolViewComponent},

  //family
    {path: "family",component: FamiliesListComponent  },
    {path: "family/new",component: FamiliesNewComponent },
    {path: "family/view",component: FamiliesViewComponent },

 //actividades
  {path: 'actividades', component: HistoryComponent},

  //login
  {path: 'login', component: LoginComponent},
  { path: '', component: LoginComponent },
  { path: '**', redirectTo: '' }

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routerComponents = [
   MainpanelComponent,
   LocalListComponent,
   LocalNewComponent,
   LocalViewComponent,
   LayoutsListComponent,
   LayoutsNewComponent,
   ProductsListComponent,
   ProductsNewComponent,
   UsersListComponent,
   UsersNewComponent,
   RolLisComponent,
   RolNewComponent,
   FamiliesListComponent,
   FamiliesNewComponent,  
   HistoryComponent,
   LayoutComponent,
   RolViewComponent, 
   ProductsViewComponent
   
   
];
 

  

