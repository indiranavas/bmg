import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;

import { LocalServiceService } from '../../Service/local/local-service.service';
import { Local } from '../../Models/local.model';
import { TokenStorageService } from '../../auth/token-storage.service';



@Component({
  selector: 'app-local-list',
  templateUrl: './local-list.component.html',
  styleUrls: ['./local-list.component.css']
})
export class LocalListComponent implements OnInit {

  constructor(private router:Router,private localservi: LocalServiceService,private toastr: ToastrService,private tokenStorage: TokenStorageService) { }
  locals :Local[];
  deleteLoc:Local;  
  errorMessage = '';
  accessStoreEdit: boolean = true;
   private roles: string[];
 
  ngOnInit() {
    this.searchLocal();
     if (this.tokenStorage.getToken()) {
           this.roles = this.tokenStorage.getAuthorities();
    if (this.roles[5] === '1' ) {
          this.accessStoreEdit = false;
     }
     }
  }

  searchLocal(){
    this.localservi.getLocal()
    .subscribe(data => {
    this.locals = data;    
    this.deleteLoc=this.locals[0]
  });
}

    edit(local:Local):void{
    localStorage.setItem("id",local.id.toString());
    this.router.navigate(["tienda/view"]);
  }
    
    delete(loc:Local,local:Local): void{       
      this.localservi.deleteLocal(loc) 
      .subscribe(data => {
        console.log(data)
        console.log(this.locals)
        this.locals = this.locals.filter(p=>p!==local);
         this.toastr.success ("Tienda eliminada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });               
      }
      );    
    }
    
    addLocal(loc:Local){        
      this.deleteLoc=loc;         
  }
}