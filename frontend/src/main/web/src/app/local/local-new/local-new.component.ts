import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';

import { LocalServiceService } from '../../Service/local/local-service.service';
import { Local } from '../../Models/local.model';
import { Scale } from '../../Models/scale.model';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-local-new',
  templateUrl: './local-new.component.html',
  styleUrls: ['./local-new.component.css']
})
export class LocalNewComponent implements OnInit {

  local: Local = new Local()
  scale : Scale= new Scale()
  scaleSelect :any = []
   ind : string
   scaleTemp : Scale= new Scale()
scaleId: number;
 submitted:boolean=false;
  errorMessage = '';
  
      localForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,     
    ]),description: new FormControl('',[
      Validators.required
    ])
  });

  constructor(private router:Router, private localservi: LocalServiceService,private toastr: ToastrService) { }

  ngOnInit() {
       this.getScaleId();
  }

 get f() { return this.localForm.controls; }
 
   addScale(add){
       if(add != undefined){
      add.id=this.scaleId
      this.scaleSelect.push(add);
      console.log(this.scaleSelect);
      this.scale = new Scale();
       this.scaleId= this.scaleId+1;
    }
  }
  
  getScaleId(){
      this.scaleId=1;
  }
  
 save(){
      this.submitted = true;
    if (this.localForm.invalid) {
      return;
    }else{
     console.log(this.local)
   this.localservi.saveLocal(this.local,this.scaleSelect)
      .subscribe(data=>{           
          this.router.navigate(["/tienda"])
          this.toastr.success ("Tienda creada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true 
      });
      },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
}

  viewScale(sca){
      console.log(sca)
      this.ind = this.scaleSelect.indexOf(sca);
      console.log( this.ind)
      this.scale= sca;
      this.scaleTemp= new Scale(sca);
  }
  
  editScale(sca){
      var indice = this.scaleSelect.indexOf(sca);
      console.log(indice) 
      console.log(this.scaleSelect)     
      this.scaleSelect.splice(this.ind,1);
      console.log(this.scaleSelect) 
      this.scaleSelect.splice(this.ind,0,sca);
      this.scale = new Scale();
 }
 
  removeScale(sca){
      var indice = this.scaleSelect.indexOf(sca); 
      this.scaleSelect.splice(indice,1);     
      console.log(sca);
  }
  
  cancel(){
     console.log(this.scaleTemp);
    this.editScale(this.scaleTemp);
 }
 
 cancelAdd(){
     this.scale= new Scale();
 }

  }