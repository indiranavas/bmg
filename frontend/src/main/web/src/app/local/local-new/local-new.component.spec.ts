import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalNewComponent } from './local-new.component';

describe('LocalNewComponent', () => {
  let component: LocalNewComponent;
  let fixture: ComponentFixture<LocalNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
