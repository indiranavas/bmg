import { Component, OnInit, ViewChild} from '@angular/core';

import { LocalServiceService } from '../../Service/local/local-service.service';
import { ScaleServiceService } from '../../Service/scale/scale-service.service';
import { Local } from '../../Models/local.model';
import { Scale } from '../../Models/scale.model';
import { Route, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms'
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-local-view',
  templateUrl: './local-view.component.html',
  styleUrls: ['./local-view.component.css']
})
export class LocalViewComponent implements OnInit {

  local: Local = new Local()
  scale : Scale= new Scale()
  scaleTemp : Scale= new Scale()
  scaleSelect :any = []
  scaleDelete :any = []
 ind : string
 scaleId: number;
 submitted:boolean=false;
  errorMessage = '';
  accessStoreEdit: boolean = true;
   private roles: string[];
  
      localForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,     
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
  
  constructor(private router:Router, private localservi: LocalServiceService,private scaleservi: ScaleServiceService,private toastr: ToastrService,private tokenStorage: TokenStorageService) { }

  ngOnInit() {
      this.edit();
      this.getScaleId() 
  if (this.tokenStorage.getToken()) {
     this.roles = this.tokenStorage.getAuthorities();
 if (this.roles[5] === '1' ) {
    this.accessStoreEdit = false;
  }
  }         
  }
  
  get f() { return this.localForm.controls; }
  
   edit(){
      let id= localStorage.getItem("id");
      this.localservi.getLocalId(id)
      .subscribe(data=>{
          this.local=data.local;
          this.scaleSelect=data.scales;
      })
  }
  
  getScaleId(){
    let id= localStorage.getItem("id");
      this.scaleservi.getScaleId(id) 
      .subscribe(data=>{
         this.scaleId=data
         console.log(this.scaleId)
      }) 
  }
  
  update(local:Local){
          this.submitted = true;
    if (this.localForm.invalid) {
      return;
    }else{
      this.localservi.updateLocal(local,this.scaleSelect)
    .subscribe(data=>{
//        this.deleteScale();
        this.local=data as any;
        this.router.navigate(["/tienda"]);
        this.toastr.success ("Tienda modificada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
  }
  }
  
  deleteScale(){
   for (let scale of this.scaleDelete) {      
      this.scaleservi.deleteScale(scale)
      .subscribe(data=>{
         console.log(scale) 
      })
  }
      
  }
     addScale(add:Scale){
      if(add != undefined){
          add.id=this.scaleId
      this.scaleSelect.push(add);
      console.log(this.scaleSelect);
      this.scale = new Scale();
      this.scaleId= this.scaleId+1;
      console.log(this.scaleId)
    }
  }
  
  removeScale(sca){
      var indice = this.scaleSelect.indexOf(sca); 
      this.scaleSelect.splice(indice,1);
      this.scaleDelete.push(sca);
      console.log(sca);
  }
  
  viewScale(sca){
      console.log(sca)
      this.ind = this.scaleSelect.indexOf(sca);
      console.log( this.ind)
      this.scale= sca;
      this.scaleTemp= new Scale(sca);
  }
  
  editScale(sca){
      var indice = this.scaleSelect.indexOf(sca);
      console.log(indice) 
      console.log(this.scaleSelect)     
      this.scaleSelect.splice(this.ind,1);
      console.log(this.scaleSelect) 
      this.scaleSelect.splice(this.ind,0,sca);
      this.scale = new Scale();
 }
 
 cancel(){
     console.log(this.scaleTemp);
    this.editScale(this.scaleTemp);
 }
 
 cancelAdd(){
     this.scale= new Scale();
 }
  
}
