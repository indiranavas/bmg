import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
 private roles: string[];
 accessActivity: boolean = true;
 accessAdministration: boolean = true;
 accessFamily: boolean = true;
 accessProduct: boolean = true;
 accessUpdateScale: boolean = true;
 accessLayout: boolean = true;
 accessStore: boolean = true;
 accessStatistics: boolean = true;
         


  
  constructor(private tokenStorage: TokenStorageService) { }

  ngOnInit() {
      if (this.tokenStorage.getToken()) {
           this.roles = this.tokenStorage.getAuthorities();
    
        if (this.roles[0] === '0' ) {
          this.accessActivity = false;    
        }
          if (this.roles[1] === '0' ) {
          this.accessFamily = false;
        }
          if (this.roles[2] === '0' ) {
          this.accessLayout = false;        
        }
          if (this.roles[3] === '0' ) {
          this.accessProduct = false;
        }
          if (this.roles[4] === '0' ) {
          this.accessStatistics = false;
        }
          if (this.roles[5] === '0' ) {
          this.accessStore = false;
     }
          if (this.roles[6] === '0' ) {
          this.accessUpdateScale = false;
        }
          if (this.roles[7] === '0' ) {
          this.accessAdministration = false;
        }

      }
  }

}