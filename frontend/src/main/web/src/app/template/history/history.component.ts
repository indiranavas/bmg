import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';

import { HistoryService } from '../../Service/history/history.service';
import { History} from '../../Models/history.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  constructor(private historyservi: HistoryService) { }
  history :History = null;
  error : boolean = false;
  ngOnInit() {
    this.searchHistory();
  }
  searchHistory(){
    this.historyservi.getHistory().pipe(
      catchError(err =>{
        this.history = null;
        this.error = true;
        return EMPTY;
      })
    ).subscribe(history=> {
      this.history = this.history;
      this.error = false;
      console.log(this.history);
    })
  }
  

  }


