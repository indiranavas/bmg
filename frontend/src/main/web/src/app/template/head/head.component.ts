import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../../auth/token-storage.service';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.css']
})
export class HeadComponent implements OnInit {
info: any;
name:string
lastName: string

 constructor(private token: TokenStorageService) { }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      name: this.token.getName(),
      lastName:this.token.getLastname(),
      authorities: this.token.getAuthorities()
    };
    this.name=this.token.getName();
    this.lastName=this.token.getLastname();
 }

  logout() {
    this.token.signOut();
//    window.location.reload();
  }

}
