
import {Users} from "../Models/users.model";

   export class JwtResponse {
    accessToken: string;
    type: string;
    username: string;
    authorities: string[];
    userDetails:Users
}
