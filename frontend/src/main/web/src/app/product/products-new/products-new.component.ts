import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';

import { ProductService } from '../../Service/product/product.service';
import { Product } from '../../Models/product.model';
import { Route, Router } from '@angular/router';
import {ToastrService} from  'ngx-toastr' ;      

@Component({
  selector: 'app-products-new',
  templateUrl: './products-new.component.html',
  styleUrls: ['./products-new.component.css']
})
export class ProductsNewComponent implements OnInit {

  product: Product = new Product()
  selectFile : File = null;
  progress: { percentage: number } = { percentage: 0 };
  submitted:boolean=false;
  errorMessage = '';
  fileValidae: boolean = false;
  
  productoFrom = new FormGroup({
    code: new FormControl('',[
      Validators.required,
      Validators.pattern(/^[0-9]\d{0,5}$/)
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
//    submitted = false;

constructor(private router:Router, private productservi: ProductService,private toastr: ToastrService) { }
 
  ngOnInit(){
 
  }
  
    get f() { return this.productoFrom.controls; }


    onFileSelected(event){
    this.selectFile =  event.target.files[0];
    console.log(this.selectFile.type)
    if(this.selectFile.type == "image/jpeg"  || this.selectFile.type == "image/png" || this.selectFile.type== 'image/jpg' || this.selectFile.type== 'image/bmp' ){
    this.fileValidae = true  

     } else{
     alert("Formato Invalido")
  }
    }

  save(){
    this.submitted = true;
    if (this.productoFrom.invalid) {
      return;
    }else{
    if (this.selectFile!=null) {
      this.product.image = this.selectFile.name;
      console.log(this.selectFile.name);
      var formData = new FormData();
      formData.append('file',this.selectFile,this.selectFile.name);       
      this.productservi.pushFileToStorage(this.selectFile); 
    } else{
       this.product.image=" ";
    }
      this.productservi.saveProduct(this.product)
      .subscribe(data=>{ 
          console.log(data)                    
          this.router.navigate(["/product"])
          this.toastr.success ("Producto creado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });          
      },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
  }
  
  public OnlyNumbers($event) {
let regex: RegExp = new RegExp(/^[0-9]{1,}$/g);
let specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'ArrowRight','ArrowLeft'];
if (specialKeys.indexOf($event.key) !== -1) {
  return;
} else {
  if (regex.test($event.key)) {
    return true;
  } else {
    return false;
  }
}
}

public back(){
     this.router.navigate(["/product"])
}


//    console.log(producto)
//    if(producto.id==undefined || producto.id==""){
//      alert('Debe ingresar el código del producto')
//      return false
//    }
//    if(producto.description==undefined || producto.description==""){
//      alert('Debe ingresar la descripción del producto')
//      return false
//    } 
////    producto.imgExist = true;
//    producto.image = this.selectFile.name;
//    console.log(this.selectFile.name);
//    var formData = new FormData();
//    formData.append('file',this.selectFile,this.selectFile.name);   
//    this.productservi.pushFileToStorage(this.selectFile).subscribe(event => {
//      console.log(event)
//      if (event.type === HttpEventType.UploadProgress) {
//        this.progress.percentage = Math.round(100 * event.loaded / event.total);
//      } else if (event instanceof HttpResponse) {
//        
//        console.log('File is completely uploaded!');
//      }
//    });  
//    console.log(producto)
//     console.log(this.productservi.postProduct(producto))
//     
//    this.router.navigateByUrl('/product');
//  }


  }