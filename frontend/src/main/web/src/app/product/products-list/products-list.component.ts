import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {Router} from '@angular/router';
import {ToastrService} from  'ngx-toastr' ; 
import { TokenStorageService } from '../../auth/token-storage.service';


import { ProductService } from '../../Service/product/product.service';
import { Product } from '../../Models/product.model';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
    
 products :Product[];
 product:Product = new Product;
 pageActual : number = 1;
 errorMessage = '';
 search : string;
 
  constructor(private productservi: ProductService,private router:Router,private toastr: ToastrService) { 
     
  } 
 
  
  ngOnInit() {
    this.searchProduct();
  }
  
  searchProduct(){
      this.productservi.getProduct()
      .subscribe(data => {
        this.products = data;
      });
  }
  
    edit(product:Product):void{
    localStorage.setItem("id",product.id.toString());
    this.router.navigate(["product/view"]);
  }
    
    delete(product:Product): void{
      this.productservi.deleteProduct(product) 
      .subscribe(data => {
        this.products = this.products.filter(p=>p!==product); 
         this.toastr.success ("Producto eliminado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
      },
      error => {              
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    
    }
    
      addProduct(product:Product){
      this.product=product;     
  }
  
  getProduct(event){
      var inputValue = (<HTMLInputElement>document.getElementById("buscar")).value;
      console.log(inputValue)
      this.products = this.products.filter(p=>p.id!==inputValue); 
  }
  
   clear() {
    this.search = '';
  }

}