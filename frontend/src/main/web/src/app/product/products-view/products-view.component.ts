import { Component, OnInit } from '@angular/core';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import {ToastrService} from  'ngx-toastr' ; 
import { TokenStorageService } from '../../auth/token-storage.service';

//import { catchError} from 'rxjs/operators';
//import { EMPTY, from} from 'rxjs';


import { ProductService } from '../../Service/product/product.service';
import { Product } from '../../Models/product.model';
import { ActivatedRoute , Router } from '@angular/router';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.css']
})
export class ProductsViewComponent implements OnInit {
 product :Product = new Product();
 selectFile : File = null;
   submitted:boolean=false;
   errorMessage = '';
 fileValidae: boolean = false;
  accessProductEdit: boolean = true;
private roles: string[];

  productoFrom = new FormGroup({
    code: new FormControl('',[
      Validators.required,
      Validators.pattern(/^[0-9]\d{0,5}$/)
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
  
  constructor(private router:Router,private productservi: ProductService,private toastr: ToastrService, private tokenStorage: TokenStorageService) { }

 
  ngOnInit() {
   this.edit();
   if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
       if (this.roles[3] === '1' ) {
          this.accessProductEdit = false;
        }
     }
  }
  
   get f() { return this.productoFrom.controls; }
   
   onFileSelected(event){
      this.selectFile =  event.target.files[0];
      console.log(this.selectFile.type)
      if(this.selectFile.type == "image/jpeg"  || this.selectFile.type == "image/png" || this.selectFile.type== 'image/jpg' || this.selectFile.type== 'image/bmp' ){
      this.fileValidae = true  

      } else{
      alert("Formato Invalido")
  }
   }
  
  edit(){
      let id= localStorage.getItem("id");
      this.productservi.getProductId(id)
      .subscribe(data=>{
          this.product=data;
      })
  }

update(product:Product){
     this.submitted = true;
    if (this.productoFrom.invalid) {
      return;
    }else{
    if (this.selectFile!=null) {
     this.product.image = this.selectFile.name;
     console.log(this.selectFile.name);
     var formData = new FormData();
     formData.append('file',this.selectFile,this.selectFile.name);   
     this.productservi.pushFileToStorage(this.selectFile);
    }  
    this.productservi.updateProduct(product)
    .subscribe(data=>{
        this.product=data as any;
        this.router.navigate(["/product"]);
        this.toastr.success ("Producto modificado correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });         
    },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
}
}
}

 