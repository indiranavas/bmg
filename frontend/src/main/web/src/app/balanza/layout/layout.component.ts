import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';

//import { BalanzaService } from '../../Service/balanza/balanza.service';
import { Layout } from '../../Models/layouts.model';
import { LocalServiceService } from '../../Service/local/local-service.service';
import { Local } from '../../Models/local.model';
import { Scale } from '../../Models/scale.model';
import { Family } from '../../Models/family.model';
import { LayoutServiceService } from '../../Service/layout/layout-service.service';

import { UpdateScaleService } from '../../Service/UpdateScale/UpdateScale.service';
import { TokenStorageService } from '../../auth/token-storage.service';



@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  Local: Local = new Local()
  families : Family[] = []
  error : boolean = false;
  scale : Scale;
  showPanel: boolean = false;
local :Local[];
id: string;
idCollap: string;
public isCollapsed = false;
layouts :Layout[]; 
selectedLayout: boolean = false;
scales: Scale[];
scalesnew: Scale[]=[];
localSelect :Scale[]=[];
accessUpdateScale: boolean = true;
private roles: string[];

  constructor(private localservi: LocalServiceService,private layoutservi: LayoutServiceService, private updatescaleservice:UpdateScaleService, private tokenStorage: TokenStorageService) { }
            
  ngOnInit() {
    this.searchLocal();
    this.searchLayout();
   if (this.tokenStorage.getToken()) {
      this.roles = this.tokenStorage.getAuthorities();
        if (this.roles[6] === '1' ) {
          this.accessUpdateScale = false;
        }
     }
  }

  searchLocal(){
    this.localservi.getLocal()
    .subscribe(data => {
    this.local = data;
//    console.log(this.local[0].scales)
//     this.getOnline(this.local[0].scales);
   });
}
    
     delete(id): void {
      this.localservi.deleteLocal(id);
 
      }
      
    searchLayout(){
    this.layoutservi.getLayout()
    .subscribe(data => {
    this.layouts = data;
     this.families=this.layouts[0].families
    console.log(this.layouts)
   });
     
  }
     
      showDescritionAndCode(data){ // logica para mostrar y ocultar 
        this.showPanel = data;
        console.log(this.showPanel);

      }
      
      changeLayout(value){  
          console.log(value)        
        this.selectedLayout=true;
        this.families= value;      
      }
      
//      viewScale(){
//        this.localservi.getLocalId(this.id)
//        .subscribe(data=>{
//          this.local=data.local;
//          this.scaleSelect=data.scales;
//        })
//      } 
     
    getOnline(scales,item){
        this.scalesnew=[]
        for (let i = 0; i < scales.length; i++) {
            this.scale= new Scale(scales[i])
            console.log(this.scale)
            this.scalesnew.push(this.scale)
        }
        this.updatescaleservice.getOnline(this.scalesnew)
        .subscribe(data => {
           var indice = this.local.indexOf(item);
           this.local[indice].scales=data           
           console.log(this.local[indice].scales)
        });
    }
   
    addScale(scale:Scale,check){
        console.log(scale)
        if(check.target.checked)
           this.localSelect.push(scale);
        if(!check.target.checked){
           var indice = this.localSelect.indexOf(scale); 
           this.localSelect.splice(indice,1);     
        } 
        console.log(this.localSelect)      
    } 
    
    saveLayout(){
        console.log(this.localSelect)
         console.log(this.families)
        this.updatescaleservice.saveLayout(this.localSelect,this.families)
        .subscribe(data => {
            console.log(this.localSelect)
        });
    }
}