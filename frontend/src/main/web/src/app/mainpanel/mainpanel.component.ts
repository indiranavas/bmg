import { Component, OnInit } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';

import { HistoryService } from '../Service/history/history.service';
import { History} from '../Models/history.model';
import { TokenStorageService } from '../auth/token-storage.service';


@Component({
  selector: 'app-mainpanel',
  templateUrl: './mainpanel.component.html',
  styleUrls: ['./mainpanel.component.css']
})
export class MainpanelComponent implements OnInit {

  constructor(private toastr: ToastrService,private historyservi: HistoryService, private tokenStorage: TokenStorageService) { }
  history :History = null;
  error : boolean = false;
  submitted:boolean=false;
  errorMessage = '';
  private roles: string[];
  accessStatistics: boolean = true;
  
  ngOnInit() {
   this.searchHistory();
    if (this.tokenStorage.getToken()) {
           this.roles = this.tokenStorage.getAuthorities();
           if (this.roles[4] === '0' ) {
          this.accessStatistics = false;
        }
    }
  }
  searchHistory(){
    this.historyservi.getHistory().pipe(
      catchError(err =>{
        this.history = null;
        this.error = true;
        return EMPTY;
      })
    ).subscribe(history=> {
      this.history = this.history;
      this.error = false;
      console.log(this.history);
    })
  }

}