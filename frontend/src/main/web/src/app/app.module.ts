import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';    
import {ToastrModule} from 'ngx-toastr';  
import { AppRoutingModule, routerComponents} from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination'; 
import { FiltersPipe } from './filters.pipe';
import { SearchFilterPipe } from './search.pipe';
import { AppComponent } from './app.component';
import { HeadComponent } from './template/head/head.component';
import { NavbarComponent } from './template/navbar/navbar.component';
import { FooterComponent } from './template/footer/footer.component';
import { FamiliesListComponent } from './families-list/families-list.component';
import { FamiliesNewComponent } from './families-new/families-new.component';
import { FamiliesViewComponent } from './families-view/families-view.component';

import { FamilyServiceService } from './Service/family/family-service.service';
import { LayoutServiceService } from './Service/layout/layout-service.service';

import {LayoutsViewComponent} from  './layout/layouts-view/layouts-view.component';
import {ProductsViewComponent} from  './product/products-view/products-view.component';
import {LoginComponent} from  './users/login/login.component';
import {RolViewComponent  } from  './users/rol/rol-view/rol-view.component';
import {UsersViewComponent } from  './users/users-view/users-view.component';

import { httpInterceptorProviders } from './auth/auth-interceptor';
import { EnvServiceProvider } from './Service/env.service.provider';

@NgModule({
  declarations: [
    AppComponent ,
    routerComponents,
    HeadComponent,
    NavbarComponent,
    FooterComponent,
    FamiliesListComponent,
    FamiliesNewComponent,
    FamiliesViewComponent,
    LayoutsViewComponent,
    ProductsViewComponent,
    
    RolViewComponent,
    UsersViewComponent,
    LoginComponent,
    FiltersPipe,
    SearchFilterPipe

  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,  
    ToastrModule.forRoot (),
    FormsModule
  ],
   

  providers: [EnvServiceProvider,httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
