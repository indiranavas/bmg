import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { catchError} from 'rxjs/operators';
import { EMPTY, from} from 'rxjs';
import {ToastrService} from  'ngx-toastr' ;
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';


import { FamilyServiceService } from '../Service/family/family-service.service';
import { ProductService } from '../Service/product/product.service';
import { Product } from '../Models/product.model';
import { Family } from '../Models/family.model';
import { Route, Router } from '@angular/router';
import { EnvService } from '../Service/env.service';

	
//import {FormBuilder, Validators,ReactiveFormsModule} from '@angular/forms'


@Component({
  selector: 'app-families-new',
  templateUrl: './families-new.component.html',
  styleUrls: ['./families-new.component.css']
})
export class FamiliesNewComponent implements OnInit {
 form;
  family: Family = new Family()
  selectProduct: Product[];
  url:string;
  products :Product[];
  selectProTemp: Product[];
  selectFile : File = null;
 product: Product = new Product
 display='none';
 submitted:boolean=false;
  errorMessage = '';
  theCheckbox = false;
   productTemp: Product = new Product
    search : string;
  fileValidae: boolean = false;
  
    familyForm = new FormGroup({
    code: new FormControl('',[
      Validators.required,
      Validators.pattern(/^[0-9]\d{0,5}$/)
    ]),description: new FormControl('',[
      Validators.required
    ])
  });
  

  constructor(private router:Router, private familyservi: FamilyServiceService,private productservi: ProductService,private toastr: ToastrService,private env: EnvService) {
    }

  ngOnInit() {
this.searchProduct();
    this.selectProduct = [];
    this.selectProTemp = [];
     this.url=this.env.apiUrl;
  }
  
   get f() { return this.familyForm.controls; }
  
    searchProduct(){
      this.productservi.getProduct()
      .subscribe(data => {
        this.products = data;
        console.log(this.products)
      });
  }
  
   onFileSelected(event){
    this.selectFile =  event.target.files[0];
    console.log(this.selectFile.type)
    if(this.selectFile.type == "image/jpeg"  || this.selectFile.type == "image/png" || this.selectFile.type== 'image/jpg' || this.selectFile.type== 'image/bmp' ){
        this.fileValidae = true  
  
    } else{
        alert("Formato Invalido")
    }

  }

save(){
     this.submitted = true;
    if (this.familyForm.invalid) {
      return;
    }else{
     if (this.selectFile!=null) {
  this.family.image = this.selectFile.name;
  var formData = new FormData();
  formData.append('file',this.selectFile,this.selectFile.name);   
  this.familyservi.pushFileToStorage(this.selectFile,this.url)
  .subscribe(data=>{  
      
  })
  }else{
         this.family.image=""
     }
  this.familyservi.saveFamily(this.family,this.selectProduct)
     .subscribe(data=>{           
          this.router.navigate(["/family"])
          this.toastr.success ("Familia creada correctamente!", 'Success',{
            timeOut: 2000,
            closeButton: true
        });        
      },
      error => {    
          console.log(error)           
        this.errorMessage = error.error.text; 
        this.toastr.error (this.errorMessage, 'Error',{
            timeOut: 2000,
            closeButton: true
        });                
      }
      );
    }
}
  
   addProducts(){
      this.selectProduct = [];
       for (let i = 0; i < this.selectProTemp.length; i++) {
           this.selectProTemp[i].checked=false 
       this.selectProduct.push(this.selectProTemp[i])
          }
      console.log(this.selectProduct);
  }
  
   addProductoTemp(add, check){   
    if(add != undefined){
        add.checked=true  
      if(check.target.checked)      
        this.selectProTemp.push(add);
      if(!check.target.checked){
           add.checked=false
        this.selectProTemp=this.selectProTemp.filter(function(value){
          return value != add;
        });
      }
        console.log(this.selectProTemp);
    }
  }
  
    openModalDialog(){
    this.display='block'; //Set block css
 }
 
  clearProducts(){
       this.selectProTemp = [];              
       for (let i = 0; i <  this.products.length; i++) {
       this.products[i].checked=false;       
 }
 console.log( this.products)
}  

  removeProduct(pro){
      var indice = this.selectProduct.indexOf(pro); 
      this.selectProduct.splice(indice,1);
          this.selectProTemp=this.selectProTemp.filter(function(value){
          return value != pro;
        });
//      this.scaleDelete.push(sca);

  }
  
   addProduct(product:Product){
      this.product=product;     
  }
  
    public OnlyNumbers($event) {
let regex: RegExp = new RegExp(/^[0-9]{1,}$/g);
let specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home', 'ArrowRight','ArrowLeft'];
if (specialKeys.indexOf($event.key) !== -1) {
  return;
} else {
  if (regex.test($event.key)) {
    return true;
  } else {
    return false;
  }
}
}

     changeProduct(){
      var indice = this.selectProduct.indexOf(this.product); 
      this.selectProduct.splice(indice,1);
      console.log(this.selectProduct)
      this.productTemp.checked=false;
      this.selectProduct.splice(indice,0,this.productTemp)
      this.selectProTemp=[]
      for (let i = 0; i < this.selectProduct.length; i++) {
         this.selectProTemp.push(this.selectProduct[i])
      }
      console.log(this.selectProduct)       
     
}

changeProductTemp(productTemp,check){
      if(check.target.checked)
      productTemp.checked=true
     if(!check.target.checked)
           productTemp.checked=false 
      this.productTemp=productTemp      
  }
  
    clear() {
  this.search = '';
}
  
  }

