import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamiliesNewComponent } from './families-new.component';

describe('FamiliesNewComponent', () => {
  let component: FamiliesNewComponent;
  let fixture: ComponentFixture<FamiliesNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamiliesNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamiliesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
